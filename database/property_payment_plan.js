const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "property_payment_plan",
   {
      pppid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("pppid", "PPP" + val);
         }
      },
      pid: { type: Sequelize.STRING(126) },
      square_meters: { type: Sequelize.STRING(126) },
      outright_plan: { type: Sequelize.STRING(126) },
      initial_deposit: { type: Sequelize.STRING(126) },
      three_months_plan: { type: Sequelize.STRING(126) , defaultValue: 'Yes' },
      six_months_plan: { type: Sequelize.STRING(126) , defaultValue: 'Yes' },
      twelve_months_plan: { type: Sequelize.STRING(126) , defaultValue: 'Yes' },
      eighteen_months_plan: { type: Sequelize.STRING(126) , defaultValue: 'Yes' },
      twentyfour_months_plan: { type: Sequelize.STRING(126) , defaultValue: 'Yes' }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "pppid",
   charset: "numeric",
   length: 7
});



module.exports = User;
