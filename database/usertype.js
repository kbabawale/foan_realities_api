const sequelize = require("./db");
const Sequelize = require("sequelize");


const UserType = sequelize.define(
   "user_type",
   {
      name: { type: Sequelize.STRING(126)  }
   },
   {
      timestamps: true
   }
);

let count = ['Admin','Customer','Realtor'];

count.forEach((role, index)=>{
   UserType.findOne({where:{name:role}}).then(found => {
      if(found){
         //continue;
      }else{
         UserType.create({
            name: role
         });
      }
   }); 
   
});


module.exports = UserType;
