const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "facilities",
   {
      fid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("fid", "F" + val);
         }
      },
      name: { type: Sequelize.STRING(126) },
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "fid",
   charset: "numeric",
   length: 7
});

let count = ['Perimeter Fencing','Street Light','Security House','Good Road Network','Drainage System','Sewage System','Power Supply','Water Supply System'];

count.forEach((role, index)=>{
   User.findOne({where:{name:role}}).then(found => {
      if(found){
         //continue;
      }else{
         User.create({
            name: role
         });
      }
   }); 
   
});



module.exports = User;