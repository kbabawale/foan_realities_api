const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const UserType = sequelize.define(
   "realtor_category",
   {
     rcid: {
        type: Sequelize.STRING(126),
        unique: true,
        primaryKey: true,
        set: function(val) {
           this.setDataValue("rcid", "RC" + val);
        }
     },
      name: { type: Sequelize.STRING(126)  }
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(UserType, {
    field: "rcid",
    charset: "numeric",
    length: 7
 });

let count = ['Bronze Realtor','Silver Realtor','Gold Realtor','Diamond Realtor','Elite Realtor'];

count.forEach((role, index)=>{
   UserType.findOne({where:{name:role}}).then(found => {
      if(found){
         //continue;
      }else{
         UserType.create({
            name: role
         });
      }
   }); 
   
});


module.exports = UserType;
