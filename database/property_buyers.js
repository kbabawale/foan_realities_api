const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "property_buyers",
   {
      pbid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("pbid", "PB" + val);
         }
      },
      cid: { type: Sequelize.STRING(126) },
      pid: { type: Sequelize.STRING(126) },
      status: { type: Sequelize.STRING(126), defaultValue: 'Unapproved' },
      bought_by: { type: Sequelize.STRING(126) },
      sold_by: {type: Sequelize.STRING(126)}
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "pbid",
   charset: "numeric",
   length: 7
});



module.exports = User;
