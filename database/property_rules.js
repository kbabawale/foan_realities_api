const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "property_rules",
   {
      prid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("prid", "PR" + val);
         }
      },
      pid: { type: Sequelize.STRING(126) },
      rule: { type: Sequelize.STRING(126) }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "prid",
   charset: "numeric",
   length: 7
});



module.exports = User;
