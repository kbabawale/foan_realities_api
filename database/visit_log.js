const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");

const User = sequelize.define(
   "visit_log",
   {
      vlid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("vlid", "VL" + val);
         }
      },
      browser: { type: Sequelize.STRING(126) },
      uid: { type: Sequelize.STRING(126) }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "vlid",
   charset: "numeric",
   length: 7
});

module.exports = User;
