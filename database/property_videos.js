const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "property_videos",
   {
      pvid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("pvid", "PV" + val);
         }
      },
      pid: { type: Sequelize.STRING(126) },
      filename: { type: Sequelize.STRING(126) }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "pvid",
   charset: "numeric",
   length: 7
});



module.exports = User;
