const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "realtors_type",
   {
      rtid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("rtid", "RT" + val);
         }
      },
      rid: { type: Sequelize.STRING(126), unique: true },
      realtor_category_id: { type: Sequelize.STRING(126) },
      amount_payable: { type: Sequelize.STRING(126) },
      commission_percent: { type: Sequelize.STRING(126) }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "rtid",
   charset: "numeric",
   length: 7
});



module.exports = User;
