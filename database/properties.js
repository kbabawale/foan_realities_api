const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "property",
   {
      pid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("pid", "P" + val);
         }
      },
      name: { type: Sequelize.STRING(126) },
      plot_size: { type: Sequelize.STRING(126) },
      price: { type: Sequelize.STRING(126) },
      no_of_plots: { type: Sequelize.STRING(126) },
      name: { type: Sequelize.STRING(126) },
      description: { type: Sequelize.STRING(126) },
      cleaning_fee: { type: Sequelize.STRING(126) },
      security_deposit: { type: Sequelize.STRING(126) },
      weekly_discount: { type: Sequelize.STRING(126) },
      monthly_discount: { type: Sequelize.STRING(126) },
      weekend_price: { type: Sequelize.STRING(126) },
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "pid",
   charset: "numeric",
   length: 7
});


module.exports = User;
