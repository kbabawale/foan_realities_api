const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "property_inspection",
   {
      piid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("piid", "PI" + val);
         }
      },
      pid: { type: Sequelize.STRING(126) },
      cid: { type: Sequelize.STRING(126) },
      inspection_date: { type: Sequelize.STRING(126) }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "piid",
   charset: "numeric",
   length: 7
});



module.exports = User;
