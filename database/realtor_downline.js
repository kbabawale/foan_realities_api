const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "realtor_downline",
   {
      rdid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("rdid", "RD" + val);
         }
      },
      rid: { type: Sequelize.STRING(126) },
      downline: { type: Sequelize.STRING(126) }
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "rdid",
   charset: "numeric",
   length: 7
});



module.exports = User;
