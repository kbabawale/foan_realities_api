const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const UserType = sequelize.define(
   "realtor_referral",
   {
     rrid: {
        type: Sequelize.STRING(126),
        unique: true,
        primaryKey: true,
        set: function(val) {
           this.setDataValue("rrid", "RR" + val);
        }
     },
      rid: { type: Sequelize.STRING(126), unique: true  },
      name: { type: Sequelize.STRING(126)  },
      phone_number: { type: Sequelize.STRING(126)  },
      email: { type: Sequelize.STRING(126)  }
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(UserType, {
    field: "rrid",
    charset: "numeric",
    length: 7
 });


module.exports = UserType;
