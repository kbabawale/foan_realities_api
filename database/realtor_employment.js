const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "realtor_employment",
   {
      reid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("reid", "RE" + val);
         }
      },
      rid: { type: Sequelize.STRING(126), unique: true },
      employer: { type: Sequelize.STRING(126) },
      designation: { type: Sequelize.STRING(126) },
      phone_number: { type: Sequelize.STRING(126) },
      address: { type: Sequelize.STRING(126) }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "reid",
   charset: "numeric",
   length: 7
});



module.exports = User;
