const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "invoices",
   {
      iid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("iid", "I" + val);
         }
      },
      pid: { type: Sequelize.STRING(126) },
      cid: { type: Sequelize.STRING(126) },
      due_date: { type: Sequelize.STRING(126) },
      description: { type: Sequelize.STRING(126) },
      quantity: { type: Sequelize.STRING(126) },
      price: { type: Sequelize.STRING(126) },
      terms_and_condition: { type: Sequelize.STRING(126) },
      invoice_number: { 
         type: Sequelize.STRING(126),
         unique: true,
         set: function(val) {
            this.setDataValue("invoice_number", "INV" + val);
         }
      }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "iid",
   charset: "numeric",
   length: 10
});

SequelizeTokenify.tokenify(User, {
    field: "invoice_number",
    charset: "numeric",
    length: 10
 });



module.exports = User;
