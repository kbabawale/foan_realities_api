const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "property_location",
   {
      plid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("plid", "PL" + val);
         }
      },
      address: { type: Sequelize.STRING(126) },
      state_id: { type: Sequelize.STRING(126) },
      
      local_govt: {
         type: Sequelize.STRING(126)
      },
      
      city: { type: Sequelize.STRING(126) },

      pid: { type: Sequelize.STRING(126), unique: true },
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "plid",
   charset: "numeric",
   length: 7
});



module.exports = User;
