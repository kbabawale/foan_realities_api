const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");

const UserType = sequelize.define(
   "role",
   {
      rid: {
        type: Sequelize.STRING(126),
        unique: true,
        primaryKey: true,
        set: function(val) {
           this.setDataValue("rid", "R" + val);
        }
     },
      name: { type: Sequelize.STRING(126)  },
      description: { type: Sequelize.STRING(126)  },
      permissions: { type: Sequelize.STRING(126)  }
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(UserType, {
   field: "rid",
   charset: "numeric",
   length: 7
});

let count = [
   {'name':'Alpha Admin', 'description':'Supreme Level Admin Account with utmost privileges'},
   {'name':'Regular Admin', 'description':'Ordinary Level Admin Account with basic privileges'}
];

count.forEach((role, index)=>{
   UserType.findOne({where:{name:role.name}}).then(found => {
      if(found){
         //continue;
      }else{
         UserType.create({
            name: role.name,
            description:role.description
         });
      }
   }); 
   
});


module.exports = UserType;
