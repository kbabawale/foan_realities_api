const sequelize = require("./db");
const Sequelize = require("sequelize");


const Countries = sequelize.define(
   "country",
   {
      name: { type: Sequelize.STRING(126) }
   },
   {
      timestamps: true
   }
);

let count = ['Afghanistan','Albania','Algeria','Andorra','Angola','Antigua and Barbuda','Argentina','Armenia','Australia','Austria','Azerbaijan',
'The Bahamas','Bahrain','Bangladesh','Barbados','Belarus','Belgium','Belize','Benin','Bhutan','Bolivia','Bosnia and Hergegovina','Botswana','Brazil',
'Brunei','Bulgaria','Burkina Faso','Burundi','Cape Verde','Cambodia','Cameroon','Canada','Central Africa Republic','Chad','Chile','China','Colombia',
'Comoros','Congo, Democratic Republic of the','Congo, Republic of the','Costa Rica','Cote d\'Ivoire','Croatia','Cuba','Cyprus','Czech Republic',
'Denmark', 'Djibouti','Dominica','Dominican Republic','East Timor','Ecuador','Egypt','El Salvador','Equatorial Guinea','Eritrea','Estonia','Ethiopia',
'Fiji','Finland','France','Gabon','The Gambia','Georgia','Germany','Ghana','Greece','Grenada','Guatemala','Guinea','Guinea-Bissau','Guyana','Haiti','Honduras',
'Hungary','Iceland','India','Indonesia','Iran','Iraq','Ireland','Israel','Italy','Jamaica','Japan','Jordan','Kazakhstan','Kenya','Kiribati','Korea, North','Korea, South',
'Kosovo','Kuwait','Kyrgystan','Laos','Latvia','Lebanon','Lesotho','Liberia','Libya','Leichtnstein','Lithuania','Luxembourg','Macedonia','Madagascar','Malawi',
'Malaysia','Maldives','Mali','Malta','Marshall Islands','Mauritania','Mauritius','Mexico','Micronesia, Federated States of','Moldova','Monaco','Mongolia',
'Montenegro','Morocco','Mozambique','Myanmar (Burma)','Namibia','Nauru','Nepal','Netherlands','New Zealand','Nicaragua','Niger','Nigeria','Norway','Oman','Pakistan',
'Palau','Panama','Papua New Guinea','Paraguay','Peru','Philippines','Poland','Portugal','Qatar','Romania','Russia','Rwanda','Saint Kitts And Nevis','Saint Lucia',
'Saint Vincent and the Grenadines','Samoa','San Marino','Sao Tome and Principe','Saudi Arabia','Senegal','Serbia','Seychelles','Sierra Leone','Singapore',
'Slovakia','Slovenia','Solomon Islands','Somalia','South Africa','Spain','Sudan','Sudan South','Suriname','Swaziland','Sweden','Switzerland','Syria',
'Taiwan','Tajikistan','Tanzania','Thailand','Togo','Tonga','Trinidad and Tobago','Tunisia','Turkey','Turkmenkistan','Tuvalu','Uganda','Ukraine',
'United Arab Emirates','United Kingdom','United States','Uruguay','Uzbekistan','Vanuatu','Vatican City','Venezuela','Vietnam','Yemen','Zambia','Zimbabwe'];

count.forEach((role, index)=>{
   Countries.findOne({where:{name:role}}).then(found => {
      if(found){
         //continue;
      }else{
         Countries.create({
            name: role
        });
      }
   });
});


module.exports = Countries;
