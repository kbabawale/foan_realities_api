const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const UserType = sequelize.define(
   "realtor_account_details",
   {
     radid: {
        type: Sequelize.STRING(126),
        unique: true,
        primaryKey: true,
        set: function(val) {
           this.setDataValue("radid", "RAD" + val);
        }
     },
      account_name: { type: Sequelize.STRING(126)  },
      rid: { type: Sequelize.STRING(126), unique: true  },
      bank_name: { type: Sequelize.STRING(126)  },
      account_number: { type: Sequelize.STRING(126)  }
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(UserType, {
    field: "radid",
    charset: "numeric",
    length: 7
 });


module.exports = UserType;
