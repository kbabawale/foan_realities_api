const sequelize = require("./db");
const Sequelize = require("sequelize");


const UserType = sequelize.define(
   "state",
   {
      name: { type: Sequelize.STRING(126)  }
   },
   {
      timestamps: true
   }
);

let count = ['Abia','Adamawa','Akwa Ibom','Anambra','Bauchi','Bayelsa','Benue','Borno','Cross River','Delta','Ebonyi','Edo','Ekiti',
'Enugu','Gombe','Imo','Jigawa','Kaduna','Kano','Katsina','kebbi','Kogi','Kwara','Lagos','Nasarawa','Niger','Ogun','Ondo','Osun','Oyo',
'Plateau','Rivers','Sokoto','Taraba','Yobe','Zamfara','FCT'];

count.forEach((role, index)=>{
   UserType.findOne({where:{name:role}}).then(found => {
      if(found){
         //continue;
      }else{
         UserType.create({
            name: role
         });
      }
   }); 
   
});


module.exports = UserType;
