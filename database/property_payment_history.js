const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "property_payment_history",
   {
      pphid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("pphid", "PPH" + val);
         }
      },
      amount: { type: Sequelize.STRING(126) },
      pbid: { type: Sequelize.STRING(126) }
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "pphid",
   charset: "numeric",
   length: 7
});



module.exports = User;
