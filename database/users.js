const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");

const User = sequelize.define(
   "user",
   {
      uid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("uid", "U" + val);
         }
      },
      firstname: { type: Sequelize.STRING(126) },
      lastname: { type: Sequelize.STRING(126) },
      
      email: {
         type: Sequelize.STRING(126)
      },
      
      user_type_id: { type: Sequelize.STRING(126) },

      password: {
         type: Sequelize.STRING(126),
         set: function(val) {
            let hash = bcryptjs.hashSync(val, 10);
            this.setDataValue("password", hash);
         }
      },
      phone_number: { type: Sequelize.STRING(126), unique: true },
      role_id: { type: Sequelize.STRING(126) },
      date_of_birth: { type: Sequelize.STRING(126) },
      address: { type: Sequelize.STRING(126) },
      mailing_address: { type: Sequelize.STRING(126) },
      status: { type: Sequelize.BOOLEAN, defaultValue: true },
      country_id: { type: Sequelize.STRING(126) },
      marital_status: { type: Sequelize.STRING(126) },
      profile_pix_filename: { type: Sequelize.STRING(126) }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "uid",
   charset: "numeric",
   length: 7
});


//check if master admin exists
User.findOne({ where: {user_type_id : 1, email: 'superadmin@gmail.com'}}).then(userfound => {
   if(userfound == null){
      //create a new user
      User.create({
         firstname: 'Super Admin',
         lastname: 'Super Admin',
         email: 'superadmin@gmail.com',
         password: 'password',
         phone_number: '080000000000',
         address: 'Dummy Address',
         user_type_id: '1',
         role_id: '1',
         marital_status: 'Single',
         country_id: '1'
      }).then(createduser => {
         uid = createduser.dataValues.id;
      });
   }
});

//check if master customer exists
User.findOne({ where: {user_type_id : 2, email: 'supercustomer@gmail.com'}}).then(userfound => {
   if(userfound == null){
      //create a new user
      User.create({
         firstname: 'Super Customer',
         lastname: 'Super Customer',
         email: 'supercustomer@gmail.com',
         password: 'password',
         phone_number: '070000000000',
         address: 'Dummy Address',
         user_type_id: '2',
         role_id: '1',
         marital_status: 'Single',
         country_id: '1'
      }).then(createduser => {
         uid = createduser.dataValues.id;
      });
   }
});

//check if master realtor exists
User.findOne({ where: {user_type_id : 3, email: 'superrealtor@gmail.com'}}).then(userfound => {
   if(userfound == null){
      //create a new user
      User.create({
         firstname: 'Super Realtor',
         lastname: 'Super Realtor',
         email: 'superrealtor@gmail.com',
         password: 'password',
         phone_number: '090000000000',
         address: 'Dummy Address',
         user_type_id: '3',
         role_id: '1',
         marital_status: 'Single',
         country_id: '1'
      }).then(createduser => {
         uid = createduser.dataValues.id;
      });
   }
});

//allow all access levels

//fetch all access levels
// AccessLevel.findAll({}).then(alfound => {
//    if(alfound.length != 0){
//       accesslevels.concat(alfound);
//    }
// });

//UserAccessLevel.create();
// accesslevels.forEach((role, index)=>{
//    UserAccessLevel.findOne({where:{user_id:uid, access_level_id: role}}).then(found => {
//       if(found){
//          //continue;
//       }else{
//          UserAccessLevel.create({
//             user_id: uid,
//             access_level_id: role
//         });
//       }
//    });
// });

module.exports = User;
