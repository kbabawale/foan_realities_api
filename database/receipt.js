const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "receipt",
   {
      reid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("reid", "RE" + val);
         }
      },
      cid: { type: Sequelize.STRING(126) },
      pid: { type: Sequelize.STRING(126) },
      due_date: { type: Sequelize.STRING(126) },
      description: { type: Sequelize.STRING(126) },
      quantity: { type: Sequelize.STRING(126) },
      price: { type: Sequelize.STRING(126) },
      terms_and_condition: { type: Sequelize.STRING(126) },
      receipt_number: { 
         type: Sequelize.STRING(126),
         unique: true,
         set: function(val) {
            this.setDataValue("receipt_number", "REC" + val);
         }
      }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "reid",
   charset: "numeric",
   length: 7
});

SequelizeTokenify.tokenify(User, {
    field: "receipt_number",
    charset: "numeric",
    length: 10
 });



module.exports = User;
