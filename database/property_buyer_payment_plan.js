const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const UserType = sequelize.define(
   "property_buyer_payment_plan",
   {
     pbppid: {
        type: Sequelize.STRING(126),
        unique: true,
        primaryKey: true,
        set: function(val) {
           this.setDataValue("pbppid", "PBPP" + val);
        }
     },
      pid: { type: Sequelize.STRING(126)  },
      cid: { type: Sequelize.STRING(126)  },
      plan: { type: Sequelize.STRING(126)  } //outright, 3-month, 6-month, 12-month, 18-month, 24-month
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(UserType, {
    field: "pbppid",
    charset: "numeric",
    length: 7
 });


module.exports = UserType;
