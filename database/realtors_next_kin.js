const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "realtors_next_of_kin",
   {
      rnkid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("rnkid", "RNK" + val);
         }
      },
      rid: { type: Sequelize.STRING(126), unique: true },
      name: { type: Sequelize.STRING(126) },
      address: { type: Sequelize.STRING(126) },
      email: { type: Sequelize.STRING(126) },
      phone_number: { type: Sequelize.STRING(126) }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "rnkid",
   charset: "numeric",
   length: 7
});



module.exports = User;
