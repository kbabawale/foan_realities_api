const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "customer_referral",
   {
      crid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("crid", "CR" + val);
         }
      },
      cid: { type: Sequelize.STRING(126), unique: true },
      name: { type: Sequelize.STRING(126) },
      email: { type: Sequelize.STRING(126) },
      phone_number: { type: Sequelize.STRING(126) }
      
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "crid",
   charset: "numeric",
   length: 7
});



module.exports = User;
