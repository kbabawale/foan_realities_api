const sequelize = require("./db");
const Sequelize = require("sequelize");
const bcryptjs = require("bcryptjs");
var SequelizeTokenify = require("sequelize-tokenify");


const User = sequelize.define(
   "property_cancellation_policy",
   {
      pcpid: {
         type: Sequelize.STRING(126),
         unique: true,
         primaryKey: true,
         set: function(val) {
            this.setDataValue("pcpid", "PCP" + val);
         }
      },
      pid: { type: Sequelize.STRING(126) },
      policy: { type: Sequelize.STRING(300) }
      
   },
   {
      timestamps: true
   }
);

SequelizeTokenify.tokenify(User, {
   field: "pcpid",
   charset: "numeric",
   length: 7
});



module.exports = User;
