//Define all the route inside the json object and present it to the app.js

class routeHandlers {
   static handleRoutes() {
      const routeHandler = {
         
         addRole: "/api/role/add",
         editRole: "/api/role/edit",
         deleteRole: "/api/role/delete",
         getRole: "/api/role/all",

         downloadFile: "/api/download",

         addPropertyLandInfo: "/api/property/land-info/add",
         editPropertyLandInfo: "/api/property/land-info/update",
         
         addPropertyLocation: "/api/property/location/add",
         editPropertyLocation: "/api/property/location/update",
         
         addPropertyFacilities: "/api/property/facilities/add",
         allPropertyFacilities: "/api/property/facilities/all",
         editPropertyFacilities: "/api/property/facilities/update",
         
         addPropertyPaymentPlan: "/api/property/payment-plan/add",
         allPropertyPaymentPlan: "/api/property/payment-plan/all",
         editPropertyPaymentPlan: "/api/property/payment-plan/update",
         deletePropertyPaymentPlan: "/api/property/payment-plan/delete",
         
         addPropertyPicture: "/api/property/picture/upload",
         
         allDbPropertyFacilities: "/api/property/facilities/db/all",
         editDbPropertyFacilities: "/api/property/facilities/db/update",
         addDbPropertyFacilities: "/api/property/facilities/db/add",
         deleteDbPropertyFacilities: "/api/property/facilities/db/delete",

         addRule: "/api/property/rules/add",
         allRule: "/api/property/rules/all",
         deleteRule: "/api/property/rules/delete",

         addCancel: "/api/property/cancellation-policy/add",
         allCancel: "/api/property/cancellation-policy/all",
         deleteCancel: "/api/property/cancellation-policy/delete",
         
         addUser: "/api/register",
         editUser: "/api/user/update",
         deleteUser: "/api/user/delete",
         getAdmin: "/api/user/admin/all",
         getCustomer: "/api/user/customer/all",
         getRealtor: "/api/user/realtor/all",
         login: "/api/login",
         editPassword: "/api/user/password/change",

         getUserType: "/api/usertype/all",

         allProperty:"/api/property/all",
         deleteProperty: "/api/property/delete",

         uploadAgreement:"/api/property/agreement/upload",
         deleteAgreement:"/api/property/agreement/delete",
         allAgreement: "/api/agreement/all",
         allCustomerAgreement: "/api/agreement/customer/all",

         buyProperty:"/api/property/buy",
         updateBuyProperty: "/api/property/buy/status/update",

         addInvoice: "/api/invoice/add",
         editInvoice: "/api/invoice/update",
         allInvoice: "/api/invoice/all",
         allRealtorInvoice: "/api/invoice/realtor/all",
         allCustomerInvoice: "/api/invoice/customer/all",
         deleteInvoice: "/api/invoice/delete",

         addReceipt: "/api/receipt/add",
         editReceipt: "/api/receipt/update",
         allReceipt: "/api/receipt/all",
         allCustomerReceipt: "/api/receipt/customer/all",
         deleteReceipt: "/api/receipt/delete",

         allState: "/api/state/all",
         allCountry: "/api/country/all",

         allPurchases:"/api/purchases/all", //all properties that have been purchased on the platform
         allCustomerPurchases:"/api/purchases/customer/all", //all properties a customer has bought for himself
 
         realtorTopPerformance: "/api/realtor/top-performance",

         addInspection: "/api/property/inspection/add",
         allInspection: "/api/property/inspection/all",
         deleteInspection: "/api/property/inspection/delete",
         allCustomerInspection: "/api/property/inspection/customer/all",
         allRealtorInspection: "/api/property/inspection/realtor/all",

         addDownline: "/api/realtor/downline/add",
         allDownline: "/api/realtor/downline/all",
         deleteDownline: "/api/realtor/downline/delete",

         realtorBank:"/api/realtor/bank-account/update",
         realtorReferral: "/api/realtor/referral/update",
         customerReferral: "/api/customer/referral/update",
         updateRealtor:"/api/realtor/info/update",
         updateCustomer: "/api/customer/info/update",

         allPropertyRealtors: "/api/property/realtors/all",
         addPropertyRealtors: "/api/property/realtors/add",
         deletePropertyRealtors: "/api/property/realtors/delete",

         getStats: "/api/property/stats/purchases",
         getStatsNewCustomers: "/api/property/stats/customers/new",
         getStatsTopCustomers: "/api/property/stats/customers/top",
         getStatsGeneral: "/api/property/stats/general",
         addVisitLog: "/api/log/visit/add",

         addPaymentHistory: "/api/property/payment/history/add",
         allPaymentHistory: "/api/property/payment/history/all",
         allCustomerPaymentHistory: "/api/property/payment/history/customer/all",
         updatePaymentHistory: "/api/property/payment/history/update",
         deletePaymentHistory: "/api/property/payment/history/delete"



      };
      return JSON.stringify(routeHandler);
   }
}

module.exports = routeHandlers;
