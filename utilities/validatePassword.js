const bcryptjs = require("bcryptjs");

let validatePassword = (password, hash) => {
   return bcryptjs.compareSync(password, hash);
};

module.exports = validatePassword;
