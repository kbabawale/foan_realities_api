const ResponseObj = require("../../utilities/responsehandler");
let Invoice = require("../../database/invoices");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { iid } = this.data;
      
        //iid validation
        if (iid == undefined || validator.isEmpty(iid)) {
            return { state: false, errorMsg: "IID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { iid } = this.data;
         Invoice.destroy({where:{iid:iid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Invoice Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
