const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {iid} = this.data;
        
        //if id provided, get its' details
        if(iid != undefined && iid != ""){
            sequelize.query("SELECT a.iid, a.pid, b.name AS property_name, a.cid, CONCAT(c.firstname, ' ', c.lastname) AS customer_name, c.phone_number, c.address, a.due_date, a.description, a.quantity, a.price, a.terms_and_condition, a.invoice_number FROM invoices a LEFT JOIN properties b ON a.pid = b.pid LEFT JOIN users c ON a.cid = c.uid WHERE iid = :iid", { replacements: {iid: iid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Invoices Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        invoices: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(iid == undefined || iid == ''){
            sequelize.query("SELECT a.iid, a.pid, b.name AS property_name, a.cid, CONCAT(c.firstname, ' ',c.lastname) AS customer_name, c.phone_number, c.address, a.due_date, a.description, a.quantity, a.price, a.terms_and_condition, a.invoice_number FROM invoices a LEFT JOIN properties b ON a.pid = b.pid LEFT JOIN users c ON a.cid = c.uid").then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Invoices Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        invoices: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }
    }
}

module.exports = Get;