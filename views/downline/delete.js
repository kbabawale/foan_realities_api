const ResponseObj = require("../../utilities/responsehandler");
let Downline = require("../../database/realtor_downline");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { rdid } = this.data;
      
        //piid validation
        if (rdid == undefined || validator.isEmpty(rdid)) {
            return { state: false, errorMsg: "RDID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { rdid } = this.data;
         Downline.destroy({where:{rdid:rdid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Downline Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
