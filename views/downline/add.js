const ResponseObj = require("../../utilities/responsehandler");
let Downline = require("../../database/realtor_downline");
let validator = require("validator");
// let cache = require("../../cache");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         rid,
         downline
      } = this.data;
      
      // rid validation
      if (
        rid == undefined || rid == '' || rid.indexOf('<') != -1 || rid.indexOf('>') != -1 || rid.indexOf('"') != -1 || rid.indexOf('\'') != -1) {
         return { state: false, errorMsg: "RID not valid." };
      }

      
      //downline validation
      if (
        downline == undefined || downline == '' || downline.indexOf('<') != -1 || downline.indexOf('>') != -1 || downline.indexOf('"') != -1 || downline.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Downline not valid." };
     }

      
      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            rid,
            downline
         } = this.data;

         Downline.create({
            rid:rid,
            downline:downline
         })
            .then(role => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Downline Added",
                  inspection: role
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
