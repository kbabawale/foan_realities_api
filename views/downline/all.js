const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {rid} = this.data;

        var finalDownlinesArr = {}; var counter = 0;
        
        
        //if id provided, get its' details
        if(rid != undefined && rid != ""){
            //load downlines
            sequelize.query("SELECT a.rdid, a.createdAt, a.updatedAt, a.downline, CONCAT(d.firstname, ' ', d.lastname) AS realtor_name, d.email AS realtor_email, d.phone_number AS realtor_phone_number FROM realtor_downlines a LEFT JOIN users d ON a.downline = d.uid WHERE rid = :rid", { replacements: {rid: rid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Downline Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    //save them
                    counter=1;
                    finalDownlinesArr['Generation_'+counter] = results;
                    
                    results.forEach((v,i)=>{
                       //for each downline, fetch lower generation downlines
                        sequelize.query("SELECT a.rdid, a.createdAt, a.updatedAt, a.downline, CONCAT(d.firstname, ' ', d.lastname) AS realtor_name, d.email AS realtor_email, d.phone_number AS realtor_phone_number FROM realtor_downlines a LEFT JOIN users d ON a.downline = d.uid WHERE rid = :rid", { replacements: {rid: v.downline} }).then(([details, metadata]) => {
                            if(details == null || details.length == 0){
                                
                                
                            }else{
                                counter=2;
                                
                                finalDownlinesArr['Generation_'+counter] = {...finalDownlinesArr['Generation_'+counter], ...details};
                                finalDownlinesArr['Generation_'+counter] = Object.values(finalDownlinesArr['Generation_'+counter]);
                                
                                //third generation
                                details.forEach((v2,i2)=>{
                                    sequelize.query("SELECT a.rdid, a.createdAt, a.updatedAt, a.downline, CONCAT(d.firstname, ' ', d.lastname) AS realtor_name, d.email AS realtor_email,d.phone_number AS realtor_phone_number FROM realtor_downlines a LEFT JOIN users d ON a.downline = d.uid WHERE rid = :rid", { replacements: {rid: v2.downline} }).then(([details2, metadata]) => {
                                        if(details2 == null || details2.length == 0){
                                            
                                            
                                        }else{
                                            counter=3;
                                
                                            finalDownlinesArr['Generation_'+counter] = {...finalDownlinesArr['Generation_'+counter], ...details2};
                                            finalDownlinesArr['Generation_'+counter] = Object.values(finalDownlinesArr['Generation_'+counter]);

                                            if(results.length-1 == i){
                                                let responseData = JSON.stringify({
                                                    downlines: finalDownlinesArr
                                                });
                                                return ResponseObj.responseHandlers(200, this.res, responseData);
                                            }
                                        }
                                    });
                                });
                                
    
                                
                            }
                        });
                    });//foreach
                }
            });
        }else {
            let responseData = JSON.stringify({
                statusMsg: "No Downline Found"
            });
            return ResponseObj.responseHandlers(400, this.res, responseData);
        }
    }
}

module.exports = Get;