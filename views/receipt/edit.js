const ResponseObj = require("../../utilities/responsehandler");
let Receipt = require("../../database/receipt");
let validator = require("validator");
// let cache = require("../../cache");

class Edit {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.edit();
   }

   validateData() {
      
        const {
            reid,
            due_date,
            description,
            quantity,
            price,
            terms_and_condition
        } = this.data;
      
      //reid validation
      if (reid == undefined) {
        return { state: false, errorMsg: "REID must be provided" };
     }

     //due_date validation
     if (
        (due_date != undefined && due_date == '') || (due_date != undefined && (due_date.indexOf('<') != -1 || due_date.indexOf('>') != -1 || due_date.indexOf('"') != -1 || due_date.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "Due Date is invalid." };
     }

     //description validation
     if (
        (description != undefined && description == '') || (description != undefined && (description.indexOf('<') != -1 || description.indexOf('>') != -1 || description.indexOf('"') != -1 || description.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "Description is invalid." };
     }

     //quantity validation
     if (
        (quantity != undefined && quantity == '') || (quantity != undefined && (quantity.indexOf('<') != -1 || quantity.indexOf('>') != -1 || quantity.indexOf('"') != -1 || quantity.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "Quantity is invalid." };
     }

     //price validation
     if (
        (price != undefined && price == '') || (price != undefined && (price.indexOf('<') != -1 || price.indexOf('>') != -1 || price.indexOf('"') != -1 || price.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "Price is invalid." };
     }
     //terms_and_condition validation
     if (
        (terms_and_condition != undefined && terms_and_condition == '') || (terms_and_condition != undefined && (terms_and_condition.indexOf('<') != -1 || terms_and_condition.indexOf('>') != -1 || terms_and_condition.indexOf('"') != -1 || terms_and_condition.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "Terms and Condition is invalid." };
     }
     

     return { state: true };
   
   }
   

   edit() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            reid,
            due_date,
            description,
            quantity,
            price,
            terms_and_condition
        } = this.data;

        Receipt.update(
         {
            due_date:due_date,
            description:description,
            quantity:quantity,
            price:price,
            terms_and_condition:terms_and_condition
        },
         {where: {reid:reid}}
        ).then(updated=>{
         let responseData = JSON.stringify({
            statusMsg: "Receipt Updated Successfully."
         });
         return ResponseObj.responseHandlers(200, this.res, responseData);
        });
         
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Edit;
