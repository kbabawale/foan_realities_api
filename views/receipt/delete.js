const ResponseObj = require("../../utilities/responsehandler");
let Receipt = require("../../database/receipt");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { reid } = this.data;
      
        //iid validation
        if (reid == undefined || validator.isEmpty(reid)) {
            return { state: false, errorMsg: "REID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { reid } = this.data;
         Receipt.destroy({where:{reid:reid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Receipt Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
