const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {reid} = this.data;
        
        //if id provided, get its' details
        if(reid != undefined && reid != ""){
            sequelize.query("SELECT a.reid, a.cid, CONCAT(c.firstname,' ', c.lastname) AS customer_name, a.due_date, a.description, a.quantity, a.price, a.terms_and_condition, a.receipt_number FROM receipts a LEFT JOIN users c ON a.cid = c.uid WHERE reid = :reid", { replacements: {reid: reid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Receipt Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        receipt: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(reid == undefined || reid == ''){
            sequelize.query("SELECT a.reid, a.cid, CONCAT(c.firstname,' ', c.lastname) AS customer_name, a.due_date, a.description, a.quantity, a.price, a.terms_and_condition, a.receipt_number FROM receipts a LEFT JOIN users c ON a.cid = c.uid").then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Receipt Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        receipt: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }
    }
}

module.exports = Get;