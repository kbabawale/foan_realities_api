const ResponseObj = require("../../utilities/responsehandler");
let Receipt = require("../../database/receipt");
let validator = require("validator");
// let cache = require("../../cache");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         cid,
         pid,
         due_date,
         description,
         quantity,
         price,
         terms_and_condition
      } = this.data;
      
      // cid validation
      if (
        cid == undefined || cid == '' || cid.indexOf('<') != -1 || cid.indexOf('>') != -1 || cid.indexOf('"') != -1 || cid.indexOf('\'') != -1) {
         return { state: false, errorMsg: "CID not valid." };
      }

      //pid validation
      if (
         pid == undefined || pid == '' || pid.indexOf('<') != -1 || pid.indexOf('>') != -1 || pid.indexOf('"') != -1 || pid.indexOf('\'') != -1) {
          return { state: false, errorMsg: "PID not valid." };
       }

      //description validation
      if (
         description == undefined || description == '' || description.indexOf('<') != -1 || description.indexOf('>') != -1 || description.indexOf('"') != -1 || description.indexOf('\'') != -1) {
        return { state: false, errorMsg: "Description not valid." };
      }

      //due_date validation
      if (
        due_date == undefined || due_date == '' || due_date.indexOf('<') != -1 || due_date.indexOf('>') != -1 || due_date.indexOf('"') != -1 || due_date.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Due Date not valid." };
     }

     //quantity validation
     if (
        quantity == undefined || quantity == '' || quantity.indexOf('<') != -1 || quantity.indexOf('>') != -1 || quantity.indexOf('"') != -1 || quantity.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Quantity not valid." };
     }

     //price validation
     if (
        price == undefined || price == '' || price.indexOf('<') != -1 || price.indexOf('>') != -1 || price.indexOf('"') != -1 || price.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Description not valid." };
     }

     //terms_and_condition validation
     if (
        terms_and_condition == undefined || terms_and_condition == '' || terms_and_condition.indexOf('<') != -1 || terms_and_condition.indexOf('>') != -1 || terms_and_condition.indexOf('"') != -1 || terms_and_condition.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Terms and Condition not valid." };
     }

      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            cid,
            due_date,
            description,
            quantity,
            price,
            pid,
            terms_and_condition
         } = this.data;

         Receipt.create({
            cid:cid,
            pid:pid,
            due_date:due_date,
            description:description,
            quantity:quantity,
            price:price,
            terms_and_condition:terms_and_condition
         })
            .then(role => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Receipt Created",
                  receipt: role
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
