const ResponseObj = require("../../utilities/responsehandler");
let Property_History = require("../../database/property_payment_history");
let validator = require("validator");
// let cache = require("../../cache");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         pbid,
         amount
      } = this.data;
      
      // pbid validation
      if (
        pbid == undefined || pbid == '' || pbid.indexOf('<') != -1 || pbid.indexOf('>') != -1 || pbid.indexOf('"') != -1 || pbid.indexOf('\'') != -1) {
         return { state: false, errorMsg: "PBID not valid." };
      }

      //amount validation
      if (
        amount == undefined || amount == '' || amount.indexOf('<') != -1 || amount.indexOf('>') != -1 || amount.indexOf('"') != -1 || amount.indexOf('\'') != -1) {
        return { state: false, errorMsg: "Amount not valid." };
      }
      
      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            pbid,
            amount
         } = this.data;

         Property_History.create({
            pbid:pbid,
            amount:amount
         })
            .then(role => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Payment History Added"
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
