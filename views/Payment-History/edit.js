const ResponseObj = require("../../utilities/responsehandler");
let Property_History = require("../../database/property_payment_history");
let validator = require("validator");

class Edit {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.edit();
   }

   validateData() {
      
    const {
        pphid,
        amount,
        pbid
    } = this.data;
      
      //rid validation
      if (pphid == undefined) {
        return { state: false, errorMsg: "PPHID must be provided" };
     }

     //amount validation
     if (
        (amount != undefined && amount == '') || (amount != undefined && (amount.indexOf('<') != -1 || amount.indexOf('>') != -1 || amount.indexOf('"') != -1 || amount.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "Amount is invalid." };
     }

     //pbid validation
     if (
        (pbid != undefined && pbid == '') || (pbid != undefined && (pbid.indexOf('<') != -1 || pbid.indexOf('>') != -1 || pbid.indexOf('"') != -1 || pbid.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "PBID is invalid." };
     }

     return { state: true };
   
   }
   

   edit() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            pphid,
            amount,
            pbid
        } = this.data;

        Property_History.update(
         {amount:amount,
            pbid: pbid
        },
         {where: {pphid:pphid}}
        ).then(updated=>{
         let responseData = JSON.stringify({
            statusMsg: "Payment History Updated Successfully."
         });
         return ResponseObj.responseHandlers(200, this.res, responseData);
        });
         
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Edit;
