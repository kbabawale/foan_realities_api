const ResponseObj = require("../../utilities/responsehandler");
let Property_History = require("../../database/property_payment_history");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { pphid } = this.data;
      
        //pphid validation
        if (pphid == undefined || validator.isEmpty(pphid)) {
            return { state: false, errorMsg: "PPHID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { pphid } = this.data;
         Property_History.destroy({where:{pphid:pphid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Property History Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
