const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {pid} = this.data;
        
        //if id provided, get its' details
        if(pid != undefined && pid != ""){
            sequelize.query("SELECT a.pphid, a.amount, a.pbid, a.createdAt, c.name, c.price, b.status,FORMAT(a.amount/c.price*100, 2) AS covered FROM property_payment_histories a, property_buyers b, properties c WHERE a.pbid = b.pbid AND b.pid = c.pid AND b.pid = :pid ORDER BY a.createdAt DESC", { replacements: {pid: pid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Payment History Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        history: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(pid == undefined || pid == ''){
            sequelize.query("SELECT a.pphid, a.amount, a.pbid, a.createdAt, c.name, c.price, b.status,FORMAT(a.amount/c.price*100, 2) AS covered FROM property_payment_histories a, property_buyers b, properties c WHERE a.pbid = b.pbid AND b.pid = c.pid ORDER BY a.createdAt DESC").then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Payment History Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        history: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }
    }
}

module.exports = Get;