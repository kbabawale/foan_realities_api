const ResponseObj = require("../../utilities/responsehandler");
let State = require("../../database/state");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const { id } = this.data;

        //if id provided, get its' details
        if(id != undefined && id != ""){
            State.findOne({ where: {id : id}}).then(type => {
                if(!type){
                    let responseData = JSON.stringify({
                        statusMsg: "No State Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                    
                }else{
                    let responseData = JSON.stringify({
                        state: type
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(id == undefined || id == ""){
            //else get all of them
            State.findAll({}).then(type => {
                if(type.length != 0){
                    let responseData = JSON.stringify({
                        state: type
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        statusMsg: "No State Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }
            });
        }
        
    }
}

module.exports = Get;