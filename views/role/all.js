const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {rid} = this.data;
        
        //if id provided, get its' details
        if(rid != undefined && rid != ""){
            sequelize.query("SELECT * FROM roles WHERE rid = :rid", { replacements: {rid: rid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Roles Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        roles: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(rid == undefined || rid == ''){
            sequelize.query("SELECT * FROM roles").then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Roles Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        roles: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }
    }
}

module.exports = Get;