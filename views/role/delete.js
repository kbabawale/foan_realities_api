const ResponseObj = require("../../utilities/responsehandler");
let Role = require("../../database/role");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { rid } = this.data;
      
        //rid validation
        if (rid == undefined || validator.isEmpty(rid)) {
            return { state: false, errorMsg: "RID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { rid } = this.data;
         Role.destroy({where:{rid:rid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Role Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
