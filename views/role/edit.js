const ResponseObj = require("../../utilities/responsehandler");
let Role = require("../../database/role");
let validator = require("validator");

class Edit {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.edit();
   }

   validateData() {
      
        const {
            rid,
            name,
            description,
            permissions
        } = this.data;
      
      //rid validation
      if (rid == undefined) {
        return { state: false, errorMsg: "RID must be provided" };
     }

     //name validation
     if (
        (name != undefined && name == '') || (name != undefined && (name.indexOf('<') != -1 || name.indexOf('>') != -1 || name.indexOf('"') != -1 || name.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "Role name is invalid." };
     }

     //description validation
     if (
        (description != undefined && description == '') || (description != undefined && (description.indexOf('<') != -1 || description.indexOf('>') != -1 || description.indexOf('"') != -1 || description.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "Description is invalid." };
     }

     //permissions validation
     if (
        (permissions != undefined && permissions == '') || (permissions != undefined && (permissions.indexOf('<') != -1 || permissions.indexOf('>') != -1 || permissions.indexOf('"') != -1 || permissions.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "Permissions is invalid." };
     }
     

     return { state: true };
   
   }
   

   edit() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            rid,
            name,
            description,
            permissions
        } = this.data;

        Role.update(
         {name:name,
            description: description,
            permissions:permissions
        },
         {where: {rid:rid}}
        ).then(updated=>{
         let responseData = JSON.stringify({
            statusMsg: "Role Updated Successfully."
         });
         return ResponseObj.responseHandlers(200, this.res, responseData);
        });
         
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Edit;
