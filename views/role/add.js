const ResponseObj = require("../../utilities/responsehandler");
let Role = require("../../database/role");
let validator = require("validator");
// let cache = require("../../cache");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         name,
         description,
         permissions
      } = this.data;
      
      // name validation
      if (
         name == undefined || name == '' || name.indexOf('<') != -1 || name.indexOf('>') != -1 || name.indexOf('"') != -1 || name.indexOf('\'') != -1) {
         return { state: false, errorMsg: "Role name not valid." };
      }

      //description validation
      if (
         description == undefined || description == '' || description.indexOf('<') != -1 || description.indexOf('>') != -1 || description.indexOf('"') != -1 || description.indexOf('\'') != -1) {
        return { state: false, errorMsg: "Description not valid." };
      }

      //permissions validation
      if (
        permissions == undefined || permissions == '' || permissions.indexOf('<') != -1 || permissions.indexOf('>') != -1 || permissions.indexOf('"') != -1 || permissions.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Permissions not valid." };
     }

      
      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            name,
            description,
            permissions
         } = this.data;

         Role.create({
            name:name,
            description:description,
            permissions:permissions
         })
            .then(role => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Role Created",
                  role: role
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
