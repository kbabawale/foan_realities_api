const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        sequelize.query("SELECT count(*) AS properties FROM properties").then(([results1, metadata]) => {
            sequelize.query("SELECT count(*) AS customers FROM users WHERE user_type_id = '2'").then(([results2, metadata]) => {
                sequelize.query("SELECT count(*) AS realtors FROM users WHERE user_type_id = '3'").then(([results3, metadata]) => {
                    let responseData = JSON.stringify({
                        customers: results2[0].customers,
                        realtors: results3[0].realtors,
                        properties: results1[0].properties
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                });
            });
        });
        
    }
}

module.exports = Get;