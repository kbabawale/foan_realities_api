const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        sequelize.query("SELECT a.uid, a.firstname, a.lastname, a.phone_number, a.email, a.profile_pix_filename, count(b.pbid) AS property_count FROM users a, property_buyers b WHERE uid IN (SELECT cid FROM property_buyers GROUP BY cid ORDER BY COUNT(cid) DESC) AND b.cid IN (SELECT cid FROM property_buyers GROUP BY cid ORDER BY COUNT(cid) DESC) AND a.user_type_id = 2 LIMIT 2").then(([results, metadata]) => {
            if(results == null || results.length == 0){
                let responseData = JSON.stringify({
                    statusMsg: "No Customers Found"
                });
                return ResponseObj.responseHandlers(400, this.res, responseData);
            }else{
                let responseData = JSON.stringify({
                    user: results
                });
                return ResponseObj.responseHandlers(200, this.res, responseData);
            }
        });
        
    }
}

module.exports = Get;