const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        //recent purchases
        var finalarr = [];
        
        //one by one
        sequelize.query("SELECT a.pbid, a.cid, CONCAT(b.firstname,' ',b.lastname) AS customer_name, a.pid, a.status, a.bought_by, a.sold_by, DATE_FORMAT(a.createdAt, '%d %M %Y') AS createdAt, a.updatedAt FROM property_buyers a LEFT JOIN users b ON a.cid = b.uid WHERE a.createdAt >= DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) AND a.createdAt <= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY a.createdAt DESC LIMIT 5").then(([re, metadata]) => {
            var arrlength = re.length;
            
            re.forEach((value,i)=>{
                var res = String(re[i].pid);
                sequelize.query("SELECT * FROM properties WHERE pid = :pid", { replacements: {pid: res} }).then(([results, metadata]) => {
                    if(results != null || results.length > 0){
                        //fetch dependent objects
                        //fetch property location
                        sequelize.query("SELECT a.plid, a.address, a.state_id, b.name AS state, a.local_govt, a.city, a.createdAt, a.updatedAt FROM property_locations a LEFT JOIN states b ON a.state_id = b.id WHERE pid = :pid", { replacements: {pid: res} }).then(([location_results, metadata]) => {
                            //fetch property facilities
                            sequelize.query("SELECT a.fid, b.name FROM property_facilities a LEFT JOIN facilities b ON a.fid = b.fid WHERE pid = :pid", { replacements: {pid: res} }).then(([facilities_results, metadata]) => {
                            
                                //fetch property payment plan
                                sequelize.query("SELECT * FROM property_payment_plans WHERE pid = :pid", { replacements: {pid: res} }).then(([payment_plan_results, metadata]) => {
                                
                                    //fetch property image
                                    sequelize.query("SELECT * FROM property_pictures WHERE pid = :pid", { replacements: {pid: res} }).then(([image_results, metadata]) => {
                                        
                                        //fetch property rules
                                        sequelize.query("SELECT * FROM property_rules WHERE pid = :pid", { replacements: {pid: res} }).then(([rules_results, metadata]) => {
                                            
                                            //fetch property cancellation policy
                                            sequelize.query("SELECT * FROM property_cancellation_policies WHERE pid = :pid", { replacements: {pid: res} }).then(([cancel_results, metadata]) => {
                                                
                                                //fetch property documents
                                                sequelize.query("SELECT * FROM property_documents WHERE pid = :pid", { replacements: {pid: res} }).then(([agreement_results, metadata]) => {
                                                    var property_details = {};
                                                    property_details.purchases_details = value;
                                                    property_details.land_info = results;
                                                    property_details.location = location_results;
                                                    property_details.payment_plan = payment_plan_results;
                                                    property_details.images = image_results;
                                                    property_details.rules = rules_results;
                                                    property_details.cancellation_policy = cancel_results;
                                                    property_details.agreements = agreement_results;

                                                    finalarr.push(property_details);
                                                    if(arrlength-1 == i){
                                                        let responseData = JSON.stringify({
                                                            purchased_properties: finalarr
                                                        });
                                                        return ResponseObj.responseHandlers(200, this.res, responseData);
                                                    }
                                                });
                                                
                                            });
                                        });
                                    });
                                });
                            });
                        });
                        
                    }
                });
            });
        });
        
        //new customers
        
        
    }
}

module.exports = Get;