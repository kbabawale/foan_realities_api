const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        //new customers
        
        sequelize.query("SELECT a.uid, a.firstname, a.lastname, a.email, a.phone_number, b.name AS user_type, a.user_type_id, c.name AS role, a.role_id, DATE_FORMAT(a.date_of_birth, '%d %M %Y')AS date_of_birth, a.address, a.mailing_address, a.status, d.name AS country, a.country_id, a.marital_status, a.profile_pix_filename, DATE_FORMAT(a.createdAt, '%d %M %Y')AS createdAt, a.updatedAt FROM users a LEFT JOIN user_types b ON a.user_type_id = b.id LEFT JOIN roles c ON a.role_id = c.rid LEFT JOIN countries d ON a.country_id = d.id WHERE a.user_type_id = 2 AND a.createdAt >= DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) AND a.createdAt <= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY a.createdAt DESC LIMIT 5").then(([results, metadata]) => {
            if(results == null || results.length == 0){
                let responseData = JSON.stringify({
                    statusMsg: "No Customers Found"
                });
                return ResponseObj.responseHandlers(400, this.res, responseData);
            }else{
                let responseData = JSON.stringify({
                    user: results
                });
                return ResponseObj.responseHandlers(200, this.res, responseData);
            }
        });
        
        //new customers
        
        
    }
}

module.exports = Get;