const ResponseObj = require("../../utilities/responsehandler");
let VisitLog = require("../../database/visit_log");
let validator = require("validator");
// let cache = require("../../cache");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         browser,
         uid
      } = this.data;
      
      // browser validation
      if (
        browser == undefined || browser == '' || browser.indexOf('<') != -1 || browser.indexOf('>') != -1 || browser.indexOf('"') != -1 || browser.indexOf('\'') != -1) {
         return { state: false, errorMsg: "Browser not valid." };
      }

      //uid validation
      if (
         uid == undefined || uid == '' || uid.indexOf('<') != -1 || uid.indexOf('>') != -1 || uid.indexOf('"') != -1 || uid.indexOf('\'') != -1) {
        return { state: false, errorMsg: "UID not valid." };
      }

      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            browser,
            uid
         } = this.data;

         VisitLog.create({
            browser: browser,
            uid: uid
         })
            .then(role => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Log Added"
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;