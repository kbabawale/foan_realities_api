const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {cid} = this.data;

        var finalarr = []; var res ='';

        //if cid provided, get its' details
        if(cid != undefined && cid != ""){
            sequelize.query("SELECT * FROM properties WHERE pid IN (SELECT pid FROM property_buyers WHERE cid = :cid)", { replacements: {cid:cid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Customer Purchased Property Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    //var res = String(results[0].pid);
                    
                    results.forEach((v,i)=>{
                        //res = String(v.pid);
                        //console.log(res, i);
                        //fetch dependent objects
                        //fetch property location
                        sequelize.query("SELECT a.plid, a.address, a.state_id, b.name AS state, a.local_govt, a.city, a.createdAt, a.updatedAt FROM property_locations a LEFT JOIN states b ON a.state_id = b.id WHERE pid = :pid", { replacements: {pid: v.pid} }).then(([location_results, metadata]) => {
                            
                            //fetch property facilities
                            sequelize.query("SELECT a.fid, b.name FROM property_facilities a LEFT JOIN facilities b ON a.fid = b.fid WHERE pid = :pid", { replacements: {pid: v.pid} }).then(([facilities_results, metadata]) => {
                                
                                //fetch property payment plan
                                sequelize.query("SELECT * FROM property_payment_plans WHERE pid = :pid", { replacements: {pid: v.pid} }).then(([payment_plan_results, metadata]) => {
                                    
                                    //fetch property image
                                    sequelize.query("SELECT * FROM property_pictures WHERE pid = :pid", { replacements: {pid: v.pid} }).then(([image_results, metadata]) => {
                                        
                                        //fetch property rules
                                        sequelize.query("SELECT * FROM property_rules WHERE pid = :pid", { replacements: {pid: v.pid} }).then(([rules_results, metadata]) => {
                                            
                                            //fetch property cancellation policy
                                            sequelize.query("SELECT * FROM property_cancellation_policies WHERE pid = :pid", { replacements: {pid: v.pid} }).then(([cancel_results, metadata]) => {
                                                
                                                //fetch property documents
                                                sequelize.query("SELECT * FROM property_documents WHERE pid = :pid", { replacements: {pid: v.pid} }).then(([agreement_results, metadata]) => {
                                                
                                                    var property_details = {};
                                                    property_details.land_info = v;
                                                    property_details.location = location_results;
                                                    property_details.facilities = facilities_results;
                                                    property_details.payment_plan = payment_plan_results;
                                                    property_details.images = image_results;
                                                    property_details.rules = rules_results;
                                                    property_details.cancellation_policy = cancel_results;
                                                    property_details.agreements = agreement_results;

                                                    finalarr.push(property_details);
                                                    if(results.length-1 == i){
                                                        let responseData = JSON.stringify({
                                                            purchased_properties: finalarr
                                                        });
                                                        return ResponseObj.responseHandlers(200, this.res, responseData); 
                                                    }

                                                       
                                                });
                                                
                                            });
                                        });
                                    });
                                });
                            });
                        });
                   
                    });
                        
                    
                    
                }
            });
        }else {
            let responseData = JSON.stringify({
                statusMsg: "No Customer Purchases Found"
            });
            return ResponseObj.responseHandlers(400, this.res, responseData); 
        }
    }
}

module.exports = Get;