const ResponseObj = require("../../../utilities/responsehandler");
const sequelize = require("../../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {pid} = this.data;
        
        //if pid provided, get its' details
        if(pid != undefined && pid != ""){
            sequelize.query("SELECT * FROM property_payment_plans WHERE pid = :pid", { replacements: {pid: pid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Payment Plans Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        plans: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else{
            let responseData = JSON.stringify({
                statusMsg: "Provide PID"
            });
            return ResponseObj.responseHandlers(400, this.res, responseData);
        }
    }
}

module.exports = Get;