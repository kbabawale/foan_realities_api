const ResponseObj = require("../../../utilities/responsehandler");
let Property_Payment_Plan = require("../../../database/property_payment_plan");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { pppid } = this.data;
      
        //pppid validation
        if (pppid == undefined || validator.isEmpty(pppid)) {
            return { state: false, errorMsg: "pppid must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { pppid } = this.data;
         Property_Payment_Plan.destroy({where:{pppid:pppid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Payment Plan Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
