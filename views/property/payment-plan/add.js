const ResponseObj = require("../../../utilities/responsehandler");
let Property_Payment_Plan = require("../../../database/property_payment_plan");
let validator = require("validator");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         pid,
         square_meters,
         outright_plan,
         initial_deposit,
         three_months_plan,
         six_months_plan,
         twelve_months_plan,
         eighteen_months_plan,
         twenty_four_months_plan
      } = this.data;
      
      // square_meters validation
      if (
        square_meters == undefined || square_meters == '' || square_meters.indexOf('<') != -1 || square_meters.indexOf('>') != -1 || square_meters.indexOf('"') != -1 || square_meters.indexOf('\'') != -1) {
         return { state: false, errorMsg: "square meters not valid." };
      }

      //outright_plan validation
      if (
        outright_plan == undefined || outright_plan == '' || outright_plan.indexOf('<') != -1 || outright_plan.indexOf('>') != -1 || outright_plan.indexOf('"') != -1 || outright_plan.indexOf('\'') != -1) {
        return { state: false, errorMsg: "Outright Plan not valid." };
      }

      //initial_deposit validation
      if (
        initial_deposit == undefined || initial_deposit == '' || validator.isNumeric(initial_deposit) == false || initial_deposit.indexOf('<') != -1 || initial_deposit.indexOf('>') != -1 || initial_deposit.indexOf('"') != -1 || initial_deposit.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Initial Deposit not valid. Must be numeric" };
     }

     //three_months_plan validation
     if (
        (three_months_plan != undefined && three_months_plan == '') || (three_months_plan != undefined && (three_months_plan.indexOf('<') != -1 || three_months_plan.indexOf('>') != -1 || three_months_plan.indexOf('"') != -1 || three_months_plan.indexOf('\'') != -1))
        )
         {
        return { state: false, errorMsg: "Three Months Plan not valid." };
      }

      //six_months_plan validation
      if (
        (six_months_plan != undefined && six_months_plan == '') || (six_months_plan != undefined && (six_months_plan.indexOf('<') != -1 || six_months_plan.indexOf('>') != -1 || six_months_plan.indexOf('"') != -1 || six_months_plan.indexOf('\'') != -1))
        ) {
        return { state: false, errorMsg: "Six Months Plan not valid." };
      }

      //twelve_months_plan validation
      if (
        (twelve_months_plan != undefined && twelve_months_plan == '') || (twelve_months_plan != undefined && (twelve_months_plan.indexOf('<') != -1 || twelve_months_plan.indexOf('>') != -1 || twelve_months_plan.indexOf('"') != -1 || twelve_months_plan.indexOf('\'') != -1))
        ) {
        return { state: false, errorMsg: "Twelve Months Plan not valid." };
      }

      //eighteen_months_plan validation
      if (
        (eighteen_months_plan != undefined && eighteen_months_plan == '') || (eighteen_months_plan != undefined && (eighteen_months_plan.indexOf('<') != -1 || eighteen_months_plan.indexOf('>') != -1 || eighteen_months_plan.indexOf('"') != -1 || eighteen_months_plan.indexOf('\'') != -1))
        ) {
        return { state: false, errorMsg: "Eighteen Months Plan not valid." };
      }

      //twenty_four_months_plan validation
      if (
        (twenty_four_months_plan != undefined && twenty_four_months_plan == '') || (twenty_four_months_plan != undefined && (twenty_four_months_plan.indexOf('<') != -1 || twenty_four_months_plan.indexOf('>') != -1 || twenty_four_months_plan.indexOf('"') != -1 || twenty_four_months_plan.indexOf('\'') != -1))
        ) {
        return { state: false, errorMsg: "Twenty Four Months Plan not valid." };
      }
     

     //pid validation
     if (
        pid == undefined || pid == '' || pid.indexOf('<') != -1 || pid.indexOf('>') != -1 || pid.indexOf('"') != -1 || pid.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Pid not valid" };
     }

      
      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            pid,
            square_meters,
            outright_plan,
            initial_deposit,
            three_months_plan,
            six_months_plan,
            twelve_months_plan,
            eighteen_months_plan,
            twenty_four_months_plan
         } = this.data;

         Property_Payment_Plan.create({
            pid:pid,
            square_meters:square_meters,
            outright_plan:outright_plan,
            initial_deposit:initial_deposit,
            three_months_plan:three_months_plan,
            six_months_plan:six_months_plan,
            twelve_months_plan:twelve_months_plan,
            eighteen_months_plan:eighteen_months_plan,
            twenty_four_months_plan:twenty_four_months_plan
         })
            .then(pro_pp => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Property Payment Plan Added",
                  payment_plan: pro_pp
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
