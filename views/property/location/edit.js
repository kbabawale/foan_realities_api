const ResponseObj = require("../../../utilities/responsehandler");
let Property_Location = require("../../../database/property_location");
let validator = require("validator");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
        address,
        state_id,
        local_govt,
        city,
        plid
      } = this.data;
      
      //plid validation
      if (
        plid == undefined ||  plid == '') {
        return { state: false, errorMsg: "PLID not valid." };
      }

      // address validation
      if (
        (address != undefined && address == '') || (address != undefined && (address.indexOf('<') != -1 || address.indexOf('>') != -1 || address.indexOf('"') != -1 || address.indexOf('\'') != -1)) 
        ) {
         return { state: false, errorMsg: "Address not valid." };
      }

      //local_govt validation
      if (
        (local_govt != undefined && local_govt == '') || (local_govt != undefined && (local_govt.indexOf('<') != -1 || local_govt.indexOf('>') != -1 || local_govt.indexOf('"') != -1 || local_govt.indexOf('\'') != -1)) 
        ) {
        return { state: false, errorMsg: "Local Govt not valid." };
      }

      //city validation
      if (
        (city != undefined && city == '') || (city != undefined && (city.indexOf('<') != -1 || city.indexOf('>') != -1 || city.indexOf('"') != -1 || city.indexOf('\'') != -1))
        ) {
       return { state: false, errorMsg: "City not valid." };
     }

     //state_id validation
     if (
        (state_id != undefined && state_id == '') || (state_id != undefined && (validator.isNumeric(state_id) == false || state_id.indexOf('<') != -1 || state_id.indexOf('>') != -1 || state_id.indexOf('"') != -1 || state_id.indexOf('\'') != -1))
        ) {
       return { state: false, errorMsg: "State_id not valid. Must be numeric" };
     }

      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            address,
            state_id,
            local_govt,
            city,
            plid
         } = this.data;

         Property_Location.update({
            address:address,
            state_id:state_id,
            local_govt:local_govt,
            city:city
         }, {where: {plid:plid}})
            .then(pro => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Property Location Updated"
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
