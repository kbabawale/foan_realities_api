const ResponseObj = require("../../../utilities/responsehandler");
let Property_Location = require("../../../database/property_location");
let validator = require("validator");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         address,
         state_id,
         local_govt,
         city,
         pid
      } = this.data;
      
      // address validation
      if (
         address == undefined || address == '' || address.indexOf('<') != -1 || address.indexOf('>') != -1 || address.indexOf('"') != -1 || address.indexOf('\'') != -1) {
         return { state: false, errorMsg: "Property name not valid." };
      }

      //local_govt validation
      if (
        local_govt == undefined || local_govt == '' || local_govt.indexOf('<') != -1 || local_govt.indexOf('>') != -1 || local_govt.indexOf('"') != -1 || local_govt.indexOf('\'') != -1) {
        return { state: false, errorMsg: "Local Govt not valid." };
      }

      //city validation
      if (
        city == undefined || city == '' || city.indexOf('<') != -1 || city.indexOf('>') != -1 || city.indexOf('"') != -1 || city.indexOf('\'') != -1) {
       return { state: false, errorMsg: "City not valid." };
     }

     //state_id validation
     if (
        state_id == undefined || state_id == '' || validator.isNumeric(state_id) == false || state_id.indexOf('<') != -1 || state_id.indexOf('>') != -1 || state_id.indexOf('"') != -1 || state_id.indexOf('\'') != -1) {
       return { state: false, errorMsg: "State not valid. Must be numeric" };
     }

     //pid validation
     if (
        pid == undefined || pid == '' || pid.indexOf('<') != -1 || pid.indexOf('>') != -1 || pid.indexOf('"') != -1 || pid.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Pid not valid." };
     }

      
      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            address,
            state_id,
            local_govt,
            city,
            pid
         } = this.data;

         Property_Location.create({
            address:address,
            state_id:state_id,
            local_govt:local_govt,
            city:city,
            pid:pid
         })
            .then(pro_loc => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Property Location Added",
                  location: pro_loc
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
