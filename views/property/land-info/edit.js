const ResponseObj = require("../../../utilities/responsehandler");
let Property = require("../../../database/properties");
let validator = require("validator");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         pid, 
         name,
         plot_size,
         price,
         no_of_plots,
         description
      } = this.data;
      
      //pid validation
      if (
        pid == undefined ||  pid == '') {
        return { state: false, errorMsg: "PID not valid." };
      }

      // name validation
      if (
        (name != undefined && name == '') || (name != undefined && (name.indexOf('<') != -1 || name.indexOf('>') != -1 || name.indexOf('"') != -1 || name.indexOf('\'') != -1)) 
        ) {
         return { state: false, errorMsg: "Property name not valid." };
      }

      //description validation
      if (
        (description != undefined && description == '') || (description != undefined && (description.indexOf('<') != -1 || description.indexOf('>') != -1 || description.indexOf('"') != -1 || description.indexOf('\'') != -1)) 
        ) {
        return { state: false, errorMsg: "Description not valid." };
      }

      //plot_size validation
      if (
        (plot_size != undefined && plot_size == '') || (plot_size != undefined && (plot_size.indexOf('<') != -1 || plot_size.indexOf('>') != -1 || plot_size.indexOf('"') != -1 || plot_size.indexOf('\'') != -1))
        ) {
       return { state: false, errorMsg: "Plot Size not valid." };
     }

     //price validation
     if (
        (price != undefined && price == '') || (price != undefined && (validator.isNumeric(price) == false || price.indexOf('<') != -1 || price.indexOf('>') != -1 || price.indexOf('"') != -1 || price.indexOf('\'') != -1))
        ) {
       return { state: false, errorMsg: "Price not valid. Must be numeric" };
     }

     //no_of_plots validation
     if (
        (no_of_plots != undefined && no_of_plots == '') || (no_of_plots != undefined && (validator.isNumeric(no_of_plots) == false || no_of_plots.indexOf('<') != -1 || no_of_plots.indexOf('>') != -1 || no_of_plots.indexOf('"') != -1 || no_of_plots.indexOf('\'') != -1))
        ) {
       return { state: false, errorMsg: "Number of Plots not valid. Must be numeric" };
     }

      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            pid,
            name,
            plot_size,
            price,
            no_of_plots,
            description
         } = this.data;

         Property.update({
            name:name,
            plot_size:plot_size,
            price:price,
            no_of_plots:no_of_plots,
            description:description
         }, {where: {pid:pid}})
            .then(pro => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Property Land Info Updated"
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
