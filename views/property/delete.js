const ResponseObj = require("../../utilities/responsehandler");
let Property = require("../../database/properties");
let Property_Location = require("../../database/property_location");
let Property_Facilities = require("../../database/property_facilities");
let Property_Payment_Plan = require("../../database/property_payment_plan");
let Property_Pictures = require("../../database/property_pictures");
let Property_Rules = require("../../database/property_rules");
let Property_Cancel = require("../../database/property_cancellation_policy");
let Property_Document = require("../../database/property_document");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { pid } = this.data;
      
        //pid validation
        if (pid == undefined || validator.isEmpty(pid)) {
            return { state: false, errorMsg: "PID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { pid } = this.data;
         Property.destroy({where:{pid:pid}}).then(deleted=>{
            Property_Rules.destroy({where:{pid:pid}}).then(deleted=>{
                Property_Location.destroy({where:{pid:pid}}).then(deleted=>{
                    Property_Cancel.destroy({where:{pid:pid}}).then(deleted=>{
                        Property_Facilities.destroy({where:{pid:pid}}).then(deleted=>{
                            Property_Payment_Plan.destroy({where:{pid:pid}}).then(deleted=>{
                                Property_Pictures.destroy({where:{pid:pid}}).then(deleted=>{
                                    Property_Document.destroy({where:{pid:pid}}).then(deleted=>{
                                       let responseData = JSON.stringify({
                                          statusMsg: "Property Deleted"
                                       });
                                       return ResponseObj.responseHandlers(200, this.res, responseData);
                                    });
                                });
                            });
                        });
                    });
                });
            });
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
