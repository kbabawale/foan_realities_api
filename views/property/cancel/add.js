const ResponseObj = require("../../../utilities/responsehandler");
let Cancel = require("../../../database/property_cancellation_policy");
let validator = require("validator");
// let cache = require("../../cache");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         policy,
         pid
      } = this.data;
      
      // policy validation
      if (
        policy == undefined || policy == '' || policy.indexOf('<') != -1 || policy.indexOf('>') != -1 || policy.indexOf('"') != -1 || policy.indexOf('\'') != -1) {
         return { state: false, errorMsg: "Policy not valid." };
      }

      //PID validation
      if (
         pid == undefined || pid == '' || pid.indexOf('<') != -1 || pid.indexOf('>') != -1 || pid.indexOf('"') != -1 || pid.indexOf('\'') != -1) {
        return { state: false, errorMsg: "PID not valid." };
      }

      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            policy,
            pid
         } = this.data;

         Cancel.create({
            policy:policy,
            pid:pid
         })
            .then(policy => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Policy Added",
                  policy: policy
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
