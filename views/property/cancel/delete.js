const ResponseObj = require("../../../utilities/responsehandler");
let Cancel = require("../../../database/property_cancellation_policy");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { pcpid } = this.data;
      
        //pcpid validation
        if (pcpid == undefined || validator.isEmpty(pcpid)) {
            return { state: false, errorMsg: "PCPID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { pcpid } = this.data;
         Cancel.destroy({where:{pcpid:pcpid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Policy Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
