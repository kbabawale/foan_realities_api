const ResponseObj = require("../../../utilities/responsehandler");
let Property_Buyer = require("../../../database/property_buyers");
let validator = require("validator");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
        pbid,
        status
      } = this.data;
      
      //pbid validation
      if (
        pbid == undefined ||  pbid == '') {
        return { state: false, errorMsg: "PBID not valid." };
      }

      // status validation
      if (
        (status != undefined && status == '') || (status != undefined && (status.indexOf('<') != -1 || status.indexOf('>') != -1 || status.indexOf('"') != -1 || status.indexOf('\'') != -1)) 
        ) {
         return { state: false, errorMsg: "Status not valid." };
      }

      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            pbid,
            status
         } = this.data;

         Property_Buyer.update({
            status:status
         }, {where: {pbid:pbid}})
            .then(pro => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Property Purchase Status Updated"
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
