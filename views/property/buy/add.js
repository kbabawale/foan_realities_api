const ResponseObj = require("../../../utilities/responsehandler");
let Property_Buyer = require("../../../database/property_buyers");
let Property_Buyer_Payment_Plan = require("../../../database/property_buyer_payment_plan");
let validator = require("validator");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         pid,
         cid,
         bought_by,
         sold_by,
         pppid
      } = this.data;
      
      //cid validation
     if (
        cid == undefined || cid == '' || cid.indexOf('<') != -1 || cid.indexOf('>') != -1 || cid.indexOf('"') != -1 || cid.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Cid not valid." };
     }
     //bought_by validation  person who clicked buy button
     if (
        bought_by == undefined || bought_by == '' || bought_by.indexOf('<') != -1 || bought_by.indexOf('>') != -1 || bought_by.indexOf('"') != -1 || bought_by.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Bought_by not valid. (uid of loggedin user)" };
     }

     //pid validation
     if (
        pid == undefined || pid == '' || pid.indexOf('<') != -1 || pid.indexOf('>') != -1 || pid.indexOf('"') != -1 || pid.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Pid not valid." };
     }

     //plan validation
     if (
      pppid == undefined || pppid == '' || pppid.indexOf('<') != -1 || pppid.indexOf('>') != -1 || pppid.indexOf('"') != -1 || pppid.indexOf('\'') != -1) {
     return { state: false, errorMsg: "Pppid not valid." };
   }
 
      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            cid,
            bought_by,
            pid,
            sold_by,
            pppid
         } = this.data;

         //prevent customer from buying same property more than once
         Property_Buyer.findOne({where:{cid:cid, pid:pid}}).then(found_dupli=>{
             if(!found_dupli){
                Property_Buyer.create({
                    bought_by:bought_by,
                    cid:cid,
                     pid:pid,
                     sold_by:sold_by
                  })
                     .then(pro_loc => {

                        //insert payment plan
                        Property_Buyer_Payment_Plan.create({
                           plan:pppid,
                           pid:pid,
                           cid:cid
                        }).then(created=>{
                           let responseData = JSON.stringify({
                              statusMsg: "Property Bought (But Not Approved)"
                              
                           });
                           return ResponseObj.responseHandlers(200, this.res, responseData);
                        });
                        
                        
                     })
                     .catch(err => {
                        let message;
                        if (err.name == "SequelizeUniqueConstraintError") {
                           message = err.errors[0].message;
                        } else {
                           message = err;
                        }
                        let responseData = JSON.stringify({
                           statusMsg: message
                        });
                        return ResponseObj.responseHandlers(400, this.res, responseData);
                     });
             }else{
                let responseData = JSON.stringify({
                    statusMsg: 'This customer has already purchased this property. Check status if it has been approved.'
                 });
                 return ResponseObj.responseHandlers(400, this.res, responseData);
             }
         });
         
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
