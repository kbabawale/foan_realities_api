const ResponseObj = require("../../../utilities/responsehandler");
let Rule = require("../../../database/property_rules");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { prid } = this.data;
      
        //prid validation
        if (prid == undefined || validator.isEmpty(prid)) {
            return { state: false, errorMsg: "PRID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { prid } = this.data;
         Rule.destroy({where:{prid:prid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Rule Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
