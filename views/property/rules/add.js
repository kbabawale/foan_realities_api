const ResponseObj = require("../../../utilities/responsehandler");
let Rule = require("../../../database/property_rules");
let validator = require("validator");
// let cache = require("../../cache");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         rule,
         pid
      } = this.data;
      
      // rule validation
      if (
        rule == undefined || rule == '' || rule.indexOf('<') != -1 || rule.indexOf('>') != -1 || rule.indexOf('"') != -1 || rule.indexOf('\'') != -1) {
         return { state: false, errorMsg: "Rule not valid." };
      }

      //PID validation
      if (
         pid == undefined || pid == '' || pid.indexOf('<') != -1 || pid.indexOf('>') != -1 || pid.indexOf('"') != -1 || pid.indexOf('\'') != -1) {
        return { state: false, errorMsg: "PID not valid." };
      }

      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            rule,
            pid
         } = this.data;

         Rule.create({
            rule:rule,
            pid:pid
         })
            .then(rule => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Rule Added",
                  rule: rule
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
