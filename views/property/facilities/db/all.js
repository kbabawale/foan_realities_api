const ResponseObj = require("../../../../utilities/responsehandler");
const sequelize = require("../../../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {fid} = this.data;
        
        //if fid provided, get its' details
        if(fid != undefined && fid != ""){
            sequelize.query("SELECT * FROM facilities WHERE fid = :fid", { replacements: {fid: fid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Facilities Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        facilities: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(fid == undefined || fid == ''){
            sequelize.query("SELECT * FROM facilities").then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Facilities Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        facilities: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }
    }
}

module.exports = Get;