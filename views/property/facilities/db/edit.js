const ResponseObj = require("../../../../utilities/responsehandler");
let Facility = require("../../../../database/facilities");
let validator = require("validator");

class Edit {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.edit();
   }

   validateData() {
      
        const {
            fid,
            name
        } = this.data;
      
      //fid validation
      if (fid == undefined) {
        return { state: false, errorMsg: "FID must be provided" };
     }

     //name validation
     if (
        (name != undefined && name == '') || (name != undefined && (name.indexOf('<') != -1 || name.indexOf('>') != -1 || name.indexOf('"') != -1 || name.indexOf('\'') != -1))) {
        return { state: false, errorMsg: "Facility name is invalid." };
     }

     
     return { state: true };
   
   }
   

   edit() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            fid,
            name
        } = this.data;

        Facility.update(
         {name:name
        },
         {where: {fid:fid}}
        ).then(updated=>{
         let responseData = JSON.stringify({
            statusMsg: "Facility Updated Successfully."
         });
         return ResponseObj.responseHandlers(200, this.res, responseData);
        });
         
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Edit;
