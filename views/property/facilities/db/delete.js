const ResponseObj = require("../../../../utilities/responsehandler");
let Facility = require("../../../../database/facilities");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { fid } = this.data;
      
        //fid validation
        if (fid == undefined || validator.isEmpty(fid)) {
            return { state: false, errorMsg: "FID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { fid } = this.data;
         Facility.destroy({where:{fid:fid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Facility Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
