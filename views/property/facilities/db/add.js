const ResponseObj = require("../../../../utilities/responsehandler");
let Facility = require("../../../../database/facilities");
let validator = require("validator");
// let cache = require("../../cache");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         name
      } = this.data;
      
      // name validation
      if (
         name == undefined || name == '' || name.indexOf('<') != -1 || name.indexOf('>') != -1 || name.indexOf('"') != -1 || name.indexOf('\'') != -1) {
         return { state: false, errorMsg: "Facility name not valid." };
      }

      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            name
         } = this.data;

         Facility.create({
            name:name
         })
            .then(facility => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Facility Created",
                  facility: facility
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
