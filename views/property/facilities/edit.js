const ResponseObj = require("../../../utilities/responsehandler");
let Property_Facilities = require("../../../database/property_facilities");
let validator = require("validator");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         facilities,
         pid
      } = this.data;
      
      // facilities validation
      if (
        facilities == undefined || facilities == '' || facilities.indexOf('<') != -1 || facilities.indexOf('>') != -1 || facilities.indexOf('"') != -1 || facilities.indexOf('\'') != -1) {
         return { state: false, errorMsg: "facilities not valid." };
      }

     //pid validation
     if (
        pid == undefined || pid == '' || pid.indexOf('<') != -1 || pid.indexOf('>') != -1 || pid.indexOf('"') != -1 || pid.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Pid not valid." };
     }

      
      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            facilities,
            pid
         } = this.data;

         //delete records of facilities for this property pid
         Property_Facilities.destroy({where:{pid:pid}}).then(deletedFacilities=>{
            //process faciities
            //convert to array
            var fac = facilities.split(',');
            if(fac.length > 0){
                fac.forEach((value, index)=>{
                //check if already exists, else create
                Property_Facilities.findOne({where: {pid:pid, fid:value}}).then(dupli=>{
                    if(!dupli){
                        Property_Facilities.create({
                            pid:pid,
                            fid:value
                        }).catch(err => {
                            let message;
                            if (err.name == "SequelizeUniqueConstraintError") {
                            message = err.errors[0].message;
                            } else {
                            message = err;
                            }
                            let responseData = JSON.stringify({
                            statusMsg: message
                            });
                            return ResponseObj.responseHandlers(400, this.res, responseData);
                        });
                    }
                });
                    
                    if(fac.length-1 == index){
                        let responseData = JSON.stringify({
                            statusMsg: "Property Facilities Updated",
                            facilities: facilities
                        });
                        return ResponseObj.responseHandlers(200, this.res, responseData);
                    } 
                });
            }
         });

            
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
