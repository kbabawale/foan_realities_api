const ResponseObj = require("../../../utilities/responsehandler");
const sequelize = require("../../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {pid} = this.data;
        
        //if pid provided, get its' details
        if(pid != undefined && pid != ""){
            sequelize.query("SELECT a.fid, b.name FROM property_facilities a LEFT JOIN facilities b ON a.fid = b.fid WHERE pid = :pid", { replacements: {pid: pid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Property Facilities Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        property_facilities: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else{
            let responseData = JSON.stringify({
                statusMsg: "Provide Property ID"
            });
            return ResponseObj.responseHandlers(400, this.res, responseData);
        }
    }
}

module.exports = Get;