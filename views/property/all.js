const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {pid} = this.data;
        var finalarr = [];
        //if pid provided, get its' details
        if(pid != undefined && pid != ""){
            sequelize.query("SELECT * FROM properties WHERE pid = :pid", { replacements: {pid: pid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Property Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    //fetch dependent objects
                    //fetch property location
                    sequelize.query("SELECT a.plid, a.address, a.state_id, b.name AS state, a.local_govt, a.city, a.createdAt, a.updatedAt FROM property_locations a LEFT JOIN states b ON a.state_id = b.id WHERE pid = :pid", { replacements: {pid: pid} }).then(([location_results, metadata]) => {

                        //fetch property facilities
                        sequelize.query("SELECT a.fid, b.name FROM property_facilities a LEFT JOIN facilities b ON a.fid = b.fid WHERE pid = :pid", { replacements: {pid: pid} }).then(([facilities_results, metadata]) => {
                           
                            //fetch property payment plan
                            sequelize.query("SELECT * FROM property_payment_plans WHERE pid = :pid", { replacements: {pid: pid} }).then(([payment_plan_results, metadata]) => {
                               
                                //fetch property image
                                sequelize.query("SELECT * FROM property_pictures WHERE pid = :pid", { replacements: {pid: pid} }).then(([image_results, metadata]) => {
                                    
                                    //fetch property rules
                                    sequelize.query("SELECT * FROM property_rules WHERE pid = :pid", { replacements: {pid: pid} }).then(([rules_results, metadata]) => {
                                        
                                        //fetch property cancellation policy
                                        sequelize.query("SELECT * FROM property_cancellation_policies WHERE pid = :pid", { replacements: {pid: pid} }).then(([cancel_results, metadata]) => {
                                            
                                            //fetch property documents
                                            sequelize.query("SELECT * FROM property_documents WHERE pid = :pid", { replacements: {pid: pid} }).then(([agreement_results, metadata]) => {

                                                //fetch assigned realtors
                                                sequelize.query("SELECT a.prid, a.pid, a.rid, CONCAT(c.firstname,' ', c.lastname) AS realtor_name, c.email, c.phone_number FROM property_realtors a LEFT JOIN users c ON a.rid = c.uid WHERE pid = :pid", { replacements: {pid: pid} }).then(([realtor_results, metadata]) => {
                                            
                                                    //fetch property purchasers
                                                    sequelize.query("SELECT a.pbid, a.pid, a.cid, CONCAT(c.firstname,' ', c.lastname) AS customer_name, c.email, c.phone_number FROM property_buyers a LEFT JOIN users c ON a.cid = c.uid WHERE pid = :pid", { replacements: {pid: pid} }).then(([buyers_results, metadata]) => {

                                                        //fetch property status sale
                                                        var sales_status = false;
                                                        sequelize.query("SELECT * FROM property_buyers WHERE pid = :pid AND status='Approved'", { replacements: {pid: pid} }).then(([sales_count, metadata]) => {
                                                            if(sales_count == null || sales_count.length == 0){
                                                                sales_status = false;
                                                            }else{
                                                                sales_status = true;
                                                            }
                                                            
                                                            var property_details = {};
                                                            property_details.land_info = results;
                                                            property_details.location = location_results;
                                                            property_details.payment_plan = payment_plan_results;
                                                            property_details.facilities = facilities_results;
                                                            property_details.images = image_results;
                                                            property_details.rules = rules_results;
                                                            property_details.cancellation_policy = cancel_results;
                                                            property_details.agreements = agreement_results;
                                                            property_details.realtors = realtor_results;
                                                            property_details.buyers = buyers_results;
                                                            property_details.sales_status = sales_status

                                                            let responseData = JSON.stringify({
                                                                property: property_details
                                                            });
                                                            return ResponseObj.responseHandlers(200, this.res, responseData); 
                                                        });  
                                                    });
                                                }); 
                                            });
                                            
                                        });
                                    });
                                });
                            });
                        });
                    });
                    
                }
            });
        }else if(pid == undefined || pid == ''){

            //one by one
            sequelize.query("SELECT pid FROM properties").then(([result, metadata]) => {
                var arrlength = result.length;
                
                result.forEach((value,i)=>{
                    // var res = String(result[i].pid);
                    var res = result[i].pid.toString();
                    sequelize.query("SELECT * FROM properties WHERE pid = :pid", { replacements: {pid: value.pid} }).then(([results, metadata]) => {
                        if(results != null){
                            //fetch dependent objects
                            //fetch property location
                            sequelize.query("SELECT a.plid, a.address, a.state_id, b.name AS state, a.local_govt, a.city, a.createdAt, a.updatedAt FROM property_locations a LEFT JOIN states b ON a.state_id = b.id WHERE pid = :pid", { replacements: {pid: value.pid} }).then(([location_results, metadata]) => {
                                //fetch property facilities
                                sequelize.query("SELECT a.fid, b.name FROM property_facilities a LEFT JOIN facilities b ON a.fid = b.fid WHERE pid = :pid", { replacements: {pid: value.pid} }).then(([facilities_results, metadata]) => {
                                
                                    //fetch property payment plan
                                    sequelize.query("SELECT * FROM property_payment_plans WHERE pid = :pid", { replacements: {pid: value.pid} }).then(([payment_plan_results, metadata]) => {
                                    
                                        //fetch property image
                                        sequelize.query("SELECT * FROM property_pictures WHERE pid = :pid", { replacements: {pid: value.pid} }).then(([image_results, metadata]) => {
                                            
                                            //fetch property rules
                                            sequelize.query("SELECT * FROM property_rules WHERE pid = :pid", { replacements: {pid: value.pid} }).then(([rules_results, metadata]) => {
                                                
                                                //fetch property cancellation policy
                                                sequelize.query("SELECT * FROM property_cancellation_policies WHERE pid = :pid", { replacements: {pid: value.pid} }).then(([cancel_results, metadata]) => {
                                                    
                                                    //fetch property documents
                                                    sequelize.query("SELECT * FROM property_documents WHERE pid = :pid", { replacements: {pid: value.pid} }).then(([agreement_results, metadata]) => {
                                                        
                                                        //fetch property realtors
                                                        sequelize.query("SELECT a.prid, a.pid, a.rid, CONCAT(c.firstname,' ', c.lastname) AS realtor_name, c.email, c.phone_number FROM property_realtors a LEFT JOIN users c ON a.rid = c.uid AND pid = :pid", { replacements: {pid: value.pid} }).then(([realtor_results, metadata]) => {

                                                            //fetch property purchasers
                                                            sequelize.query("SELECT a.pbid, a.pid, a.cid, CONCAT(c.firstname,' ', c.lastname) AS customer_name, c.email, c.phone_number FROM property_buyers a LEFT JOIN users c ON a.cid = c.uid AND pid = :pid", { replacements: {pid: value.pid} }).then(([buyers_results, metadata]) => {
                                                
                                                    
                                                                var property_details = {};
                                                                property_details.land_info = results;
                                                                property_details.location = location_results;
                                                                property_details.facilities = facilities_results;
                                                                property_details.payment_plan = payment_plan_results;
                                                                property_details.images = image_results;
                                                                property_details.rules = rules_results;
                                                                property_details.cancellation_policy = cancel_results;
                                                                property_details.agreements = agreement_results;
                                                                property_details.realtors = realtor_results;
                                                                property_details.buyers = buyers_results;

                                                                finalarr.push(property_details);
                                                                if(arrlength-1 == i){
                                                                    let responseData = JSON.stringify({
                                                                        property: finalarr
                                                                    });
                                                                    return ResponseObj.responseHandlers(200, this.res, responseData);
                                                                }

                                                            });
                                                        });
                                                    });
                                                    
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                            
                        }
                    });
                });
            });
        }
    }
}

module.exports = Get;