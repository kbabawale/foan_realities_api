const ResponseObj = require("../../utilities/responsehandler");
let Inspection = require("../../database/property_inspection");
let validator = require("validator");
// let cache = require("../../cache");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         pid,
         cid,
         inspection_date
      } = this.data;
      
      // pid validation
      if (
        pid == undefined || pid == '' || pid.indexOf('<') != -1 || pid.indexOf('>') != -1 || pid.indexOf('"') != -1 || pid.indexOf('\'') != -1) {
         return { state: false, errorMsg: "PID not valid." };
      }

      //cid validation
      if (
        cid == undefined || cid == '' || cid.indexOf('<') != -1 || cid.indexOf('>') != -1 || cid.indexOf('"') != -1 || cid.indexOf('\'') != -1) {
        return { state: false, errorMsg: "CID not valid." };
      }

      //inspection_date validation
      if (
        inspection_date == undefined || inspection_date == '' || inspection_date.indexOf('<') != -1 || inspection_date.indexOf('>') != -1 || inspection_date.indexOf('"') != -1 || inspection_date.indexOf('\'') != -1) {
       return { state: false, errorMsg: "Inspection Date not valid." };
     }

      
      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            pid,
            cid,
            inspection_date
         } = this.data;

         Inspection.create({
            pid:pid,
            cid:cid,
            inspection_date:inspection_date
         })
            .then(role => {
               
               let responseData = JSON.stringify({
                  statusMsg: "Inspection Booked",
                  inspection: role
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            })
            .catch(err => {
               let message;
               if (err.name == "SequelizeUniqueConstraintError") {
                  message = err.errors[0].message;
               } else {
                  message = err;
               }
               let responseData = JSON.stringify({
                  statusMsg: message
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
