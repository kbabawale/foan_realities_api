const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {rid} = this.data;
        
        //if id provided, get its' details
        if(rid != undefined && rid != ""){
            sequelize.query("SELECT a.piid, a.pid, b.name AS property_name, c.address AS property_address, a.cid, CONCAT(d.firstname, ' ', d.lastname) AS customer_name, a.inspection_date, DATE_FORMAT(a.createdAt, '%Y %M %d') AS createdAt FROM property_inspections a LEFT JOIN properties b ON a.pid = b.pid LEFT JOIN users d ON a.cid = d.uid LEFT JOIN property_locations c ON a.pid = c.pid WHERE a.pid IN (SELECT pid from property_buyers WHERE sold_by = :rid)", { replacements: {rid: rid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Inspection Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        inspections: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else {
            let responseData = JSON.stringify({
                statusMsg: "No Inspection Found"
            });
            return ResponseObj.responseHandlers(400, this.res, responseData);
        }
    }
}

module.exports = Get;