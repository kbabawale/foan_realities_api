const ResponseObj = require("../../utilities/responsehandler");
let Inspection = require("../../database/property_inspection");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { piid } = this.data;
      
        //piid validation
        if (piid == undefined || validator.isEmpty(piid)) {
            return { state: false, errorMsg: "PIID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { piid } = this.data;
         Inspection.destroy({where:{piid:piid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Inspection Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
