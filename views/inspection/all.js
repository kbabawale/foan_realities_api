const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {piid} = this.data;
        
        //if id provided, get its' details
        if(piid != undefined && piid != ""){
            sequelize.query("SELECT a.piid, a.pid, b.name AS property_name, c.address AS property_address, a.cid, CONCAT(d.firstname, ' ', d.lastname) AS customer_name, a.inspection_date, a.createdAt FROM property_inspections a LEFT JOIN properties b ON a.pid = b.pid LEFT JOIN users d ON a.cid = d.uid LEFT JOIN property_locations c ON a.pid = c.pid WHERE piid = :piid", { replacements: {piid: piid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Inspection Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        inspections: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(piid == undefined || piid == ''){
            sequelize.query("SELECT a.piid, a.pid, b.name AS property_name, c.address AS property_address, a.cid, CONCAT(d.firstname, ' ', d.lastname) AS customer_name, a.inspection_date, a.createdAt FROM property_inspections a LEFT JOIN properties b ON a.pid = b.pid LEFT JOIN users d ON a.cid = d.uid LEFT JOIN property_locations c ON a.pid = c.pid").then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Inspection Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        inspections: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }
    }
}

module.exports = Get;