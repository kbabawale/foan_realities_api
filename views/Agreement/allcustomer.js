const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {
        
        const {cid} = this.data;
        
        //if id provided, get its' details
        if(cid != undefined && cid != ""){
            sequelize.query("SELECT a.pdid, a.pid, a.filename, a.createdAt, a.updatedAt FROM property_documents a WHERE a.pid IN (SELECT pid FROM property_buyers WHERE cid = :cid) ", { replacements: {cid: cid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Agreements Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        agreements: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else {
            let responseData = JSON.stringify({
                statusMsg: "No Agreements Found"
            });
            return ResponseObj.responseHandlers(400, this.res, responseData);
        }
    }
}

module.exports = Get;