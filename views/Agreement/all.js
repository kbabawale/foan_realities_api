const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {pid} = this.data;
        
        //if id provided, get its' details
        if(pid != undefined && pid != ""){
            sequelize.query("SELECT * FROM property_documents WHERE pid = :pid", { replacements: {pid: pid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Agreements Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        agreements: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(pid == undefined || pid == ''){
            sequelize.query("SELECT * FROM property_documents").then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Agreements Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        agreements: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }
    }
}

module.exports = Get;