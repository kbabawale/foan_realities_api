const ResponseObj = require("../../utilities/responsehandler");
let Country = require("../../database/countries");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        
            //else get all of them
            Country.findAll({}).then(country => {
                if(country.length != 0){
                    let responseData = JSON.stringify({
                        country: country
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        statusMsg: "No Country Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }
            });
        
        
    }
}

module.exports = Get;