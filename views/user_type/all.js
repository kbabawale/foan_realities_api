const ResponseObj = require("../../utilities/responsehandler");
let UserType = require("../../database/usertype");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const { id } = this.data;

        //if id provided, get its' details
        if(id != undefined && id != ""){
            UserType.findOne({ where: {id : id}}).then(type => {
                if(!type){
                    let responseData = JSON.stringify({
                        statusMsg: "No Type Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                    
                }else{
                    let responseData = JSON.stringify({
                        type: type
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(id == undefined || id == ""){
            //else get all of them
            UserType.findAll({}).then(type => {
                if(type.length != 0){
                    let responseData = JSON.stringify({
                        type: type
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        statusMsg: "No Type Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }
            });
        }
        
    }
}

module.exports = Get;