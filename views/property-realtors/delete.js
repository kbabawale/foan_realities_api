const ResponseObj = require("../../utilities/responsehandler");
let PropertyRealtor = require("../../database/property_realtors");
let validator = require("validator");

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { rid, pid } = this.data;
      
        //rid validation
        if (rid == undefined || validator.isEmpty(rid)) {
            return { state: false, errorMsg: "RID must be provided" };
        }

        //pid validation
        if (pid == undefined || validator.isEmpty(pid)) {
            return { state: false, errorMsg: "PID must be provided" };
        }

      
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { rid,pid } = this.data;
         PropertyRealtor.destroy({where:{rid:rid, pid:pid}}).then(deleted=>{
            let responseData = JSON.stringify({
                statusMsg: "Receipt Deleted"
             });
             return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
