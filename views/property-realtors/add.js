const ResponseObj = require("../../utilities/responsehandler");
let PropertyRealtors = require("../../database/property_realtors");
let validator = require("validator");
// let cache = require("../../cache");

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         rid,
         pid
      } = this.data;
      
      // rid validation
      if (
        rid == undefined || rid == '' || rid.indexOf('<') != -1 || rid.indexOf('>') != -1 || rid.indexOf('"') != -1 || rid.indexOf('\'') != -1) {
         return { state: false, errorMsg: "RID not valid." };
      }

      // pid validation
      if (
        pid == undefined || pid == '' || pid.indexOf('<') != -1 || pid.indexOf('>') != -1 || pid.indexOf('"') != -1 || pid.indexOf('\'') != -1) {
         return { state: false, errorMsg: "PID not valid." };
      }

      
      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            rid,
            pid
         } = this.data;

         //avoid duplicates
         PropertyRealtors.findOne({where:{rid:rid}}).then(found=>{
            if(!found){
               PropertyRealtors.create({
                  pid,
                  rid
               })
                  .then(role => {
                     
                     let responseData = JSON.stringify({
                        statusMsg: "Realtor Created",
                        realtor: role
                     });
                     return ResponseObj.responseHandlers(200, this.res, responseData);
                  })
                  .catch(err => {
                     let message;
                     if (err.name == "SequelizeUniqueConstraintError") {
                        message = err.errors[0].message;
                     } else {
                        message = err;
                     }
                     let responseData = JSON.stringify({
                        statusMsg: message
                     });
                     return ResponseObj.responseHandlers(400, this.res, responseData);
                  });
            }else{
               let responseData = JSON.stringify({
                  statusMsg: "Realtor Created"
                  
               });
               return ResponseObj.responseHandlers(200, this.res, responseData);
            }
         });

         
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
