const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {pid} = this.data;
        
        //if id provided, get its' details
        if(pid != undefined && pid != ""){
            sequelize.query("SELECT a.prid, a.pid, a.rid, CONCAT(c.firstname,' ', c.lastname) AS realtor_name, c.email, c.phone_number FROM property_realtors a LEFT JOIN users c ON a.rid = c.uid WHERE pid = :pid", { replacements: {pid: pid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Realtor Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        realtors: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(pid == undefined || pid == ''){
            sequelize.query("SELECT a.prid, a.pid, a.rid, CONCAT(c.firstname,' ', c.lastname) AS realtor_name, c.email, c.phone_number FROM property_realtors a LEFT JOIN users c ON a.rid = c.uid").then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No Realtor Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        realtors: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }
    }
}

module.exports = Get;