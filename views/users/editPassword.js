const ResponseObj = require("../../utilities/responsehandler");
let Users = require("../../database/users");

class Edit {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.edit();
   }

   validateData() {
      
        const {
            uid,
            newpassword
        } = this.data;
      
      //uid validation
      if (uid == undefined) {
        return { state: false, errorMsg: "UID must be provided" };
    }

    //password validation
    if (newpassword == undefined ||
        newpassword.length < 4 ||
        newpassword.length > 15) {
        return { state: false, errorMsg: "Password must be in valid format." };
    }
      
     

     return { state: true };
   
   }
   

   edit() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            uid,
            newpassword
        } = this.data;

            Users.findOne({ where: { uid:uid } }).then(user => {
            if (user == null) {
               let responseData = JSON.stringify({
                  statusMsg: "User was not found."
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            }
            user.update({
                password: newpassword
            });
            let responseData = JSON.stringify({
               statusMsg: "User Password Updated Successfully."
            });
            return ResponseObj.responseHandlers(200, this.res, responseData);
            
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Edit;
