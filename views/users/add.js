const ResponseObj = require("../../utilities/responsehandler");
let Users = require("../../database/users");
let jwt = require("jsonwebtoken");
let customer_referral = require("../../database/customer_referral");
let customer_employment = require("../../database/customer_employment");
let customer_next_of_kin = require("../../database/customer_next_kin");

let realtor_referral = require("../../database/realtor_referral");
let realtor_bank = require("../../database/realtor_account_details");
let realtor_next_of_kin = require("../../database/realtors_next_kin");
let realtor_type = require("../../database/realtor_type");

let validator = require("validator");
// let cache = require("../../cache");

const genderRoles = ["MALE", "FEMALE"];

class Add {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doRegister();
   }

   validateData() {
      const {
         firstname,
         lastname,
         user_type_id,
         password,
         phone_number,
         email,
         role_id,
         nationality_id,
         marital_status,
         date_of_birth,
         address
      } = this.data;
      
      // firstname validation
      if (
         firstname == undefined || firstname == '' || firstname.indexOf('<') != -1 || firstname.indexOf('>') != -1 || firstname.indexOf('"') != -1 || firstname.indexOf('\'') != -1) {
         return { state: false, errorMsg: "Firstname not valid." };
      }

      //lastname validation
      if (
         lastname == undefined || lastname == '' || lastname.indexOf('<') != -1 || lastname.indexOf('>') != -1 || lastname.indexOf('"') != -1 || lastname.indexOf('\'') != -1) {
        return { state: false, errorMsg: "Lastname not valid." };
      }

      //phone_number validation
      if (
         phone_number == undefined || phone_number == '' || phone_number.indexOf('<') != -1 || phone_number.indexOf('>') != -1 || phone_number.indexOf('"') != -1 || phone_number.indexOf('\'') != -1) {
        return { state: false, errorMsg: "Phone Number not valid." };
      }

      
      // email validation
      if (
         email == undefined ||
         (validator.isEmpty(email) == false &&
            validator.isEmail(email) == false)
      ) {
         return {
            state: false,
            errorMsg: "Email invalid"
         };
      }
        // user_type_id validation
      if (
        user_type_id == undefined ||
        validator.isNumeric(user_type_id) == false ) {
        return { state: false, errorMsg: "User Type is invalid" };
     }

      // password length validation
      if (
        password == undefined ||
        password.length < 4 ||
        password.length > 15
     ) {
        return {
           state: false,
           errorMsg: "Password must be between the range of 4 and 15"
        };
     }

      return { state: true };
   }
   doRegister() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            firstname,
            lastname,
            user_type_id,
            password,
            phone_number,
            email,
            role_id,
            nationality_id,
            marital_status,
            date_of_birth,
            address
         } = this.data;

         //check for duplicates
         Users.findOne({where:{phone_number:phone_number}}).then(found=>{
            if(found){
               let responseData = JSON.stringify({
                  statusMsg: 'This Account Already Exists'
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            }else{
               Users.create({
                  firstname:firstname,
                  lastname:lastname,
                  user_type_id:user_type_id,
                  password:password,
                  email:email,
                  phone_number:phone_number,
                  role_id:role_id,
                  nationality_id:nationality_id,
                  marital_status:marital_status,
                  date_of_birth:date_of_birth,
                  address:address
               })
                  .then(users => {
      
                     if(user_type_id == 2){ //customer
                        //create record of nextofkin, referral, employment
                        customer_employment.create({
                           cid:users.dataValues.uid
                        });
                        customer_referral.create({
                           cid:users.dataValues.uid
                        });
                        customer_next_of_kin.create({
                           cid:users.dataValues.uid
                        });
      
                        // let token = jwt.sign(
                        //    {
                        //       email: email,
                        //       phone: phone,
                        //       uid: users.uid
                        //    },
                        //    process.env.SECRETKEY,
                        //    { expiresIn: 3600 }
                        // );
                        
                        delete users.dataValues.password;
      
                        let responseData = JSON.stringify({
                           statusMsg: "Registration Successfully",
                           users: users
                        });
                        return ResponseObj.responseHandlers(200, this.res, responseData);
                     }else if(user_type_id == 3){ //realtor
                        //create record of nextofkin, referral, realtor type, bank details
                        realtor_bank.create({
                           rid:users.dataValues.uid
                        });
                        realtor_next_of_kin.create({
                           rid:users.dataValues.uid
                        });
                        realtor_referral.create({
                           rid:users.dataValues.uid
                        });
                        realtor_type.create({
                           rid:users.dataValues.uid
                        });
      
                        // let token = jwt.sign(
                        //    {
                        //       email: email,
                        //       phone: phone,
                        //       uid: users.uid
                        //    },
                        //    process.env.SECRETKEY,
                        //    { expiresIn: 24800 }
                        // );
                        
                        delete users.dataValues.password;
                        let responseData = JSON.stringify({
                           statusMsg: "Registration Successfully",
                           users: users
                        });
                        return ResponseObj.responseHandlers(200, this.res, responseData);
                     }else if(user_type_id == 1){ //admin
                        // let token = jwt.sign(
                        //    {
                        //       email: email,
                        //       phone: phone,
                        //       uid: users.uid
                        //    },
                        //    process.env.SECRETKEY,
                        //    { expiresIn: 24800 }
                        // );
                        
                        delete users.dataValues.password;
                        let responseData = JSON.stringify({
                           statusMsg: "Registration Successfully",
                           users: users
                        });
                        return ResponseObj.responseHandlers(200, this.res, responseData);
                     }
                  })
                  .catch(err => {
                     let message;
                     if (err.name == "SequelizeUniqueConstraintError") {
                        message = err.errors[0].message;
                     } else {
                        message = err;
                     }
                     let responseData = JSON.stringify({
                        statusMsg: message
                     });
                     return ResponseObj.responseHandlers(400, this.res, responseData);
                  });
            }
         });
         
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Add;
