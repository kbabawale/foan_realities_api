const ResponseObj = require("../../utilities/responsehandler");
let Users = require("../../database/users");
let validator = require("validator");

class Edit {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.edit();
   }

   validateData() {
      
        const {
            uid,
            firstname,
            lastname,
            user_type_id,
            phone_number,
            email,
            role_id,
            date_of_birth,
            address,
            mailing_address,
            country_id,
            marital_status
        } = this.data;
      
      //uid validation
      if (uid == undefined) {
        return { state: false, errorMsg: "UID must be provided" };
     }

      // firstname validation
      if (
         (firstname != undefined && firstname == '') || (firstname != undefined && (firstname.indexOf('<') != -1 || firstname.indexOf('>') != -1 || firstname.indexOf('"') != -1 || firstname.indexOf('\'') != -1))
         ) {
        return { state: false, errorMsg: "Firstname not valid." };
     }

     //lastname validation
     if (
      (lastname != undefined && lastname == '') || (lastname != undefined && (lastname.indexOf('<') != -1 || lastname.indexOf('>') != -1 || lastname.indexOf('"') != -1 || lastname.indexOf('\'') != -1)) 
      ) {
       return { state: false, errorMsg: "Lastname not valid." };
     }

     
     // email validation
     if (
        (email != undefined && validator.isEmpty(email)) || (email != undefined && validator.isEmail(email) == false)
     ) {
        return {
           state: false,
           errorMsg: "Email invalid"
        };
     }

       // user_type_id validation
     if (
       (user_type_id != undefined && validator.isNumeric(user_type_id) == false ) || (user_type_id != undefined && validator.isEmpty(user_type_id))) {
       return { state: false, errorMsg: "User Type is invalid" };
    }

     // phone validation
     if ((phone_number != undefined && validator.isNumeric(phone_number) == false) || (phone_number != undefined && phone_number.length < 11) || (phone_number != undefined && phone_number.length > 15)
     ) {
        return { state: false, errorMsg: "Phone number is invalid" };
     }

     // Address validation
     if (
      (address != undefined && address == '') || (address != undefined && (address.indexOf('<') != -1 || address.indexOf('>') != -1 || address.indexOf('"') != -1 || address.indexOf('\'') != -1)
       )) {
        return { state: false, errorMsg: "Address is not valid" };
     }

     // role_id validation
     if (
      (role_id != undefined && role_id == '') || (role_id != undefined && (role_id.indexOf('<') != -1 || role_id.indexOf('>') != -1 || role_id.indexOf('"') != -1 || role_id.indexOf('\'') != -1)
       )) {
        return { state: false, errorMsg: "Role ID is not valid" };
     }

     // marital_status validation
     if (
      (marital_status != undefined && marital_status == '') || (marital_status != undefined && (marital_status.indexOf('<') != -1 || marital_status.indexOf('>') != -1 || marital_status.indexOf('"') != -1 || marital_status.indexOf('\'') != -1)
       )) {
        return { state: false, errorMsg: "Marital Status is not valid" };
     }

     // country_id validation
     if (
      (country_id != undefined && country_id == '') || (country_id != undefined && (country_id.indexOf('<') != -1 || country_id.indexOf('>') != -1 || country_id.indexOf('"') != -1 || country_id.indexOf('\'') != -1)
       )) {
        return { state: false, errorMsg: "Country ID is not valid" };
     }

     // date_of_birth validation
     if (
      (date_of_birth != undefined && date_of_birth == '') || (date_of_birth != undefined && (date_of_birth.indexOf('<') != -1 || date_of_birth.indexOf('>') != -1 || date_of_birth.indexOf('"') != -1 || date_of_birth.indexOf('\'') != -1)
       )) {
        return { state: false, errorMsg: "Date of Birth is not valid" };
     }

     // mailing_address validation
     if (
      (mailing_address != undefined && mailing_address == '') || (mailing_address != undefined && (mailing_address.indexOf('<') != -1 || mailing_address.indexOf('>') != -1 || mailing_address.indexOf('"') != -1 || mailing_address.indexOf('\'') != -1)
       )) {
        return { state: false, errorMsg: "Mailing Address is not valid" };
     }

     

     return { state: true };
   
   }
   

   edit() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            uid,
            firstname,
            lastname,
            user_type_id,
            phone_number,
            email,
            role_id,
            date_of_birth,
            address,
            mailing_address,
            country_id,
            marital_status
        } = this.data;

        Users.update(
         {
            firstname:firstname,
            lastname:lastname,
            user_type_id:user_type_id,
            phone_number:phone_number,
            email:email,
            role_id:role_id,
            date_of_birth:date_of_birth,
            address:address,
            mailing_address:mailing_address,
            country_id:country_id,
            marital_status:marital_status},
         {where: {uid:uid}}
        ).then(updated=>{
         let responseData = JSON.stringify({
            statusMsg: "User Updated Successfully."
         });
         return ResponseObj.responseHandlers(200, this.res, responseData);
        });
         
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Edit;
