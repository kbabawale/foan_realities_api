const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {uid} = this.data;
        
        //if uid provided, get its' details
        if(uid != undefined && uid != ""){
            sequelize.query("SELECT a.uid, a.firstname, a.lastname, a.email, a.phone_number, b.name AS user_type, a.user_type_id, c.name AS role, a.role_id, DATE_FORMAT(a.date_of_birth, '%Y-%m-%d')AS date_of_birth, a.address, a.mailing_address, a.status, d.name AS country, a.country_id, a.marital_status, a.profile_pix_filename, DATE_FORMAT(a.createdAt, '%d %M %Y')AS createdAt, DATE_FORMAT(a.updatedAt, '%d %M %Y')AS updatedAt FROM users a LEFT JOIN user_types b ON a.user_type_id = b.id LEFT JOIN roles c ON a.role_id = c.rid LEFT JOIN countries d ON a.country_id = d.id WHERE uid = :uid AND user_type_id = 1", { replacements: {uid: uid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No User Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        user: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }else if(uid == undefined || uid == ''){
            sequelize.query("SELECT a.uid, a.firstname, a.lastname, a.email, a.phone_number, b.name AS user_type, a.user_type_id, c.name AS role, a.role_id, DATE_FORMAT(a.date_of_birth, '%Y-%m-%d')AS date_of_birth, a.address, a.mailing_address, a.status, d.name AS country, a.country_id, a.marital_status, a.profile_pix_filename,  DATE_FORMAT(a.createdAt, '%d %M %Y')AS createdAt, DATE_FORMAT(a.updatedAt, '%d %M %Y')AS updatedAt FROM users a LEFT JOIN user_types b ON a.user_type_id = b.id LEFT JOIN roles c ON a.role_id = c.rid LEFT JOIN countries d ON a.country_id = d.id WHERE user_type_id = 1").then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No User Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        user: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }
    }
}

module.exports = Get;