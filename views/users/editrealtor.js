const ResponseObj = require("../../utilities/responsehandler");
let NextKin = require("../../database/realtors_next_kin");
let RealtorType = require("../../database/realtor_type");
let validator = require("validator");

class Edit {
    constructor(data, res) {
       this.data = data.body;
       this.res = res;
       this.edit();
    }
 
    validateData() {
       
         const {
             rid,
             name,
             email,
             phone_number,
             address,
             realtor_category_id,
             amount_payable,
             commission_percent
             
         } = this.data;
       
       //rid validation
       if (rid == undefined) {
         return { state: false, errorMsg: "RID must be provided" };
      }
 
       // email validation
      if (
         (email != undefined && validator.isEmpty(email)) || (email != undefined && validator.isEmail(email) == false)
      ) {
         return {
            state: false,
            errorMsg: "Email invalid"
         };
      }

      // Address validation
     if (
        (address != undefined && address == '') || (address != undefined && (address.indexOf('<') != -1 || address.indexOf('>') != -1 || address.indexOf('"') != -1 || address.indexOf('\'') != -1)
         )) {
          return { state: false, errorMsg: "Address is not valid" };
       }
  
 
      //name validation
      if (
         (name != undefined && name == '') || (name != undefined && (name.indexOf('<') != -1 || name.indexOf('>') != -1 || name.indexOf('"') != -1 || name.indexOf('\'') != -1)) 
         ) {
          return { state: false, errorMsg: "name not valid." };
        }
 
        // phone validation
      if ((phone_number != undefined && validator.isNumeric(phone_number) == false) || (phone_number != undefined && phone_number.length < 11) || (phone_number != undefined && phone_number.length > 15)
      ) {
         return { state: false, errorMsg: "Phone number is invalid" };
      }
 
      //realtor_category_id validation
      if (
       (realtor_category_id != undefined && realtor_category_id == '') || (realtor_category_id != undefined && (realtor_category_id.indexOf('<') != -1 || realtor_category_id.indexOf('>') != -1 || realtor_category_id.indexOf('"') != -1 || realtor_category_id.indexOf('\'') != -1)) 
       ) {
        return { state: false, errorMsg: "Realtor Category not valid." };
      }
 
      //amount_payable validation
      if (
       (amount_payable != undefined && amount_payable == '') || (amount_payable != undefined && (amount_payable.indexOf('<') != -1 || amount_payable.indexOf('>') != -1 || amount_payable.indexOf('"') != -1 || amount_payable.indexOf('\'') != -1)) 
       ) {
        return { state: false, errorMsg: "Amount Payable not valid." };
      }
 
      //commission_percent validation
      if (
       (commission_percent != undefined && commission_percent == '') || (commission_percent != undefined && (commission_percent.indexOf('<') != -1 || commission_percent.indexOf('>') != -1 || commission_percent.indexOf('"') != -1 || commission_percent.indexOf('\'') != -1)) 
       ) {
        return { state: false, errorMsg: "Commission Percent not valid." };
      }
 
      
 
      return { state: true };
    
    }
    
 
    edit() {
       let validated = this.validateData();
       if (validated.state) {
         const {
          rid,
          name,
          email,
          phone_number,
          address,
          realtor_category_id,
          amount_payable,
          commission_percent
         } = this.data;
 
         NextKin.update(
          {
              address:address,
             phone_number:phone_number,
             name:name,
             email:email},
          {where: {rid:rid}}
         ).then(updated=>{

            RealtorType.update(
                {
                    realtor_category_id:realtor_category_id,
                   amount_payable:amount_payable,
                   commission_percent:commission_percent
                   },
                {where: {rid:rid}}
               ).then(finalediting=>{
                let responseData = JSON.stringify({
                    statusMsg: "Realtor Details Updated Successfully."
                 });
                 return ResponseObj.responseHandlers(200, this.res, responseData);
               });
         });
          
       } else {
          let responseData = JSON.stringify({
             statusMsg: validated.errorMsg
          });
          return ResponseObj.responseHandlers(400, this.res, responseData);
       }
    }
 }
 
 module.exports = Edit;
 