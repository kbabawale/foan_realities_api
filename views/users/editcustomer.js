const ResponseObj = require("../../utilities/responsehandler");
let NextKin = require("../../database/customer_next_kin");
let Employment = require("../../database/customer_employment");
let validator = require("validator");

class Edit {
    constructor(data, res) {
       this.data = data.body;
       this.res = res;
       this.edit();
    }
 
    validateData() {
       
         const {
             cid,
             nk_name,
             nk_email,
             nk_phone_number,
             nk_address,
             employer,
             emp_address,
             emp_designation,
             emp_phone_number
             
         } = this.data;
       
       //cid validation
       if (cid == undefined) {
         return { state: false, errorMsg: "CID must be provided" };
      }
 
       // email validation
      if (
         (nk_email != undefined && validator.isEmpty(nk_email)) || (nk_email != undefined && validator.isEmail(nk_email) == false)
      ) {
         return {
            state: false,
            errorMsg: "Next Of Kin Email invalid"
         };
      }

      // Address validation
     if (
        (nk_address != undefined && nk_address == '') || (nk_address != undefined && (nk_address.indexOf('<') != -1 || nk_address.indexOf('>') != -1 || nk_address.indexOf('"') != -1 || nk_address.indexOf('\'') != -1)
         )) {
          return { state: false, errorMsg: "Next Of Kin Address is not valid" };
       }
  
 
      //name validation
      if (
         (nk_name != undefined && nk_name == '') || (nk_name != undefined && (nk_name.indexOf('<') != -1 || nk_name.indexOf('>') != -1 || nk_name.indexOf('"') != -1 || nk_name.indexOf('\'') != -1)) 
         ) {
          return { state: false, errorMsg: "Next Of Kin name not valid." };
        }
 
        // phone validation
      if ((nk_phone_number != undefined && validator.isNumeric(nk_phone_number) == false) || (nk_phone_number != undefined && nk_phone_number.length < 11) || (nk_phone_number != undefined && nk_phone_number.length > 15)
      ) {
         return { state: false, errorMsg: "Next Of Kin Phone number is invalid" };
      }
 
      //employer validation
      if (
       (employer != undefined && employer == '') || (employer != undefined && (employer.indexOf('<') != -1 || employer.indexOf('>') != -1 || employer.indexOf('"') != -1 || employer.indexOf('\'') != -1)) 
       ) {
        return { state: false, errorMsg: "Customer Employer not valid." };
      }
 
      //emp_address validation
      if (
       (emp_address != undefined && emp_address == '') || (emp_address != undefined && (emp_address.indexOf('<') != -1 || emp_address.indexOf('>') != -1 || emp_address.indexOf('"') != -1 || emp_address.indexOf('\'') != -1)) 
       ) {
        return { state: false, errorMsg: "Employment Address not valid." };
      }
 
      //emp_designation validation
      if (
       (emp_designation != undefined && emp_designation == '') || (emp_designation != undefined && (emp_designation.indexOf('<') != -1 || emp_designation.indexOf('>') != -1 || emp_designation.indexOf('"') != -1 || emp_designation.indexOf('\'') != -1)) 
       ) {
        return { state: false, errorMsg: "Employment Designation not valid." };
      }

      //emp_phone_number validation
      if (
        (emp_phone_number != undefined && emp_phone_number == '') || (emp_phone_number != undefined && (emp_phone_number.indexOf('<') != -1 || emp_phone_number.indexOf('>') != -1 || emp_phone_number.indexOf('"') != -1 || emp_phone_number.indexOf('\'') != -1)) 
        ) {
         return { state: false, errorMsg: "Employment Phone Number not valid." };
       }
 
      
 
      return { state: true };
    
    }
    
 
    edit() {
       let validated = this.validateData();
       if (validated.state) {
         const {
            cid,
            nk_name,
            nk_email,
            nk_phone_number,
            nk_address,
            employer,
            emp_address,
            emp_designation,
            emp_phone_number
         } = this.data;
 
         NextKin.update(
          {
            name:nk_name,
            email:nk_email,
            phone_number:nk_phone_number,
            address:nk_address},
          {where: {cid:cid}}
         ).then(updated=>{

            Employment.update(
                {
                    employer:employer,
                    address:emp_address,
                    designation:emp_designation,
                    phone_number:emp_phone_number
                   },
                {where: {cid:cid}}
               ).then(finalediting=>{
                let responseData = JSON.stringify({
                    statusMsg: "Customer Details Updated Successfully."
                 });
                 return ResponseObj.responseHandlers(200, this.res, responseData);
               });
         });
          
       } else {
          let responseData = JSON.stringify({
             statusMsg: validated.errorMsg
          });
          return ResponseObj.responseHandlers(400, this.res, responseData);
       }
    }
 }
 
 module.exports = Edit;
 