const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        sequelize.query("SELECT a.uid, a.firstname, a.lastname, a.phone_number, a.email, a.profile_pix_filename FROM users a WHERE uid IN (SELECT sold_by FROM property_buyers GROUP BY sold_by ORDER BY COUNT(sold_by) DESC) AND a.user_type_id = 3 LIMIT 2").then(([results, metadata]) => {
            if(results == null || results.length == 0){
                let responseData = JSON.stringify({
                    statusMsg: "No Realtors Found"
                });
                return ResponseObj.responseHandlers(400, this.res, responseData);
            }else{
                let responseData = JSON.stringify({
                    top_realtors: results
                });
                return ResponseObj.responseHandlers(200, this.res, responseData);
            }
        });
        
    }
}

module.exports = Get;