const ResponseObj = require("../../utilities/responsehandler");
const validatePassword = require("../../utilities/validatePassword");
let User = require("../../database/users");
let VisitLog = require("../../database/visit_log");
let jwt = require("jsonwebtoken");

class Login {
   constructor(data, res) {
      this.reqq = data;
      this.data = data.body;
      this.res = res;
      this.dologin();
   }

   validateData() {
    const {
       email,
       password
    } = this.data;
    
    
    // validation
    if (
       password == undefined || password == '' || email == undefined || email == '') {
       return { state: false, errorMsg: "Fill up the fields." };
    }

    return { state: true };
 }

   dologin() {
    let validated = this.validateData();
    if (validated.state) {
      const { email, password } = this.data;

      User.findOne({ where: { email: email } }).then(user => {
        // No record found
        if (user == null) {
           let responseData = JSON.stringify({
              statusMsg: "Login Failed"
           });
           return ResponseObj.responseHandlers(400, this.res, responseData);
        }

        // Blocked User
        if (user.dataValues.status == false) {
           let responseData = JSON.stringify({
              statusMsg: "This Account Has Been Blocked"
           });
           return ResponseObj.responseHandlers(400, this.res, responseData);
        }

        // Validate password
        if (validatePassword(password, user.password)) {
         
         let token = jwt.sign(
            {
               email: user.dataValues.email,
               mobile: user.dataValues.mobile,
               phone: user.dataValues.phone_1,
               userId: user.dataValues.uid
            },
            process.env.SECRETKEY,
            { expiresIn: 3600 } // Expires in 1 hour
         );

         delete user.dataValues.password;

            let responseData = JSON.stringify({
                statusMsg: "Login Successful",
                user: user,
                token: token
             });
             
             

             return ResponseObj.responseHandlers(
                200,
                this.res,
                responseData
             );
        } else {
           // Invalid Password
           let responseData = JSON.stringify({
              statusMsg: "Login Failed"
           });
           return ResponseObj.responseHandlers(400, this.res, responseData);
        }
     });
    }else{
        let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
    }
   }
}

module.exports = Login;
