const ResponseObj = require("../../utilities/responsehandler");
const sequelize = require("../../database/db");

class Get{
    constructor(req, res) {
        this.data = req.body;
        this.res = res;
        this.getThem();
    }
    getThem() {

        const {uid} = this.data;
        
        var finalarr = [];
        //if uid provided, get its' details
        if(uid != undefined && uid != ""){
            sequelize.query("SELECT a.uid, a.firstname, a.lastname, a.email, a.phone_number, b.name AS user_type, a.user_type_id, c.name AS role, a.role_id, DATE_FORMAT(a.date_of_birth, '%d %M %Y')AS date_of_birth, a.address, a.mailing_address, a.status, d.name AS country, a.country_id, a.marital_status, a.profile_pix_filename, DATE_FORMAT(a.createdAt, '%d %M %Y')AS createdAt, a.updatedAt FROM users a LEFT JOIN user_types b ON a.user_type_id = b.id LEFT JOIN roles c ON a.role_id = c.rid LEFT JOIN countries d ON a.country_id = d.id WHERE uid = :uid AND user_type_id = 2", { replacements: {uid: uid} }).then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No User Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{

                    //fetch customer referral
                    sequelize.query("SELECT a.crid, a.cid, a.name, a.email, a.phone_number, DATE_FORMAT(a.createdAt, '%d %M %Y')AS createdAt, DATE_FORMAT(a.updatedAt, '%d %M %Y')AS updatedAt FROM customer_referrals a WHERE a.cid = :uid ", { replacements: {uid: uid} }).then(([resultsa, metadata]) => {
                        //fetch customer nextofkin
                        sequelize.query("SELECT a.cnkid, a.cid, a.name, a.address, a.email, a.phone_number, DATE_FORMAT(a.createdAt, '%d %M %Y')AS createdAt, DATE_FORMAT(a.updatedAt, '%d %M %Y')AS updatedAt FROM customer_next_of_kins a WHERE a.cid = :uid ", { replacements: {uid: uid} }).then(([resultsb, metadata]) => {
                            //fetch customer employment
                            sequelize.query("SELECT a.ceid, a.cid, a.employer, a.designation, a.address, a.phone_number, DATE_FORMAT(a.createdAt, '%d %M %Y')AS createdAt, DATE_FORMAT(a.updatedAt, '%d %M %Y')AS updatedAt FROM customer_employments a WHERE a.cid = :uid ", { replacements: {uid: uid} }).then(([resultsc, metadata]) => {
                                var user_details = {};
                                user_details.basic_details = results;
                                user_details.referrals = resultsa;
                                user_details.next_of_kin = resultsb;
                                user_details.employment = resultsc;
                                
                                finalarr.push(user_details);
                                // if(arrlength-1 == i){
                                let responseData = JSON.stringify({
                                    user: finalarr
                                });
                                return ResponseObj.responseHandlers(200, this.res, responseData);
                                // }
                            });
                        });
                    });
                    
                    
                    
                }
            });
        }else if(uid == undefined || uid == ''){
            sequelize.query("SELECT a.uid, a.firstname, a.lastname, a.email, a.phone_number, b.name AS user_type, a.user_type_id, c.name AS role, a.role_id, DATE_FORMAT(a.date_of_birth, '%d %M %Y')AS date_of_birth, a.address, a.mailing_address, a.status, d.name AS country, a.country_id, a.marital_status, a.profile_pix_filename, DATE_FORMAT(a.createdAt, '%d %M %Y')AS createdAt, a.updatedAt FROM users a LEFT JOIN user_types b ON a.user_type_id = b.id LEFT JOIN roles c ON a.role_id = c.rid LEFT JOIN countries d ON a.country_id = d.id WHERE user_type_id = 2").then(([results, metadata]) => {
                if(results == null || results.length == 0){
                    let responseData = JSON.stringify({
                        statusMsg: "No User Found"
                    });
                    return ResponseObj.responseHandlers(400, this.res, responseData);
                }else{
                    let responseData = JSON.stringify({
                        user: results
                    });
                    return ResponseObj.responseHandlers(200, this.res, responseData);
                }
            });
        }
    }
}

module.exports = Get;