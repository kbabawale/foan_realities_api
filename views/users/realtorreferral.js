const ResponseObj = require("../../utilities/responsehandler");
let Referral = require("../../database/realtor_referral");
let validator = require("validator");

class Edit {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.edit();
   }

   validateData() {
      
        const {
            rid,
            name,
            email,
            phone_number
            
        } = this.data;
      
      //rid validation
      if (rid == undefined) {
        return { state: false, errorMsg: "RID must be provided" };
     }

      // email validation
     if (
        (email != undefined && validator.isEmpty(email)) || (email != undefined && validator.isEmail(email) == false)
     ) {
        return {
           state: false,
           errorMsg: "Email invalid"
        };
     }

     //name validation
     if (
        (name != undefined && name == '') || (name != undefined && (name.indexOf('<') != -1 || name.indexOf('>') != -1 || name.indexOf('"') != -1 || name.indexOf('\'') != -1)) 
        ) {
         return { state: false, errorMsg: "name not valid." };
       }

       // phone validation
     if ((phone_number != undefined && validator.isNumeric(phone_number) == false) || (phone_number != undefined && phone_number.length < 11) || (phone_number != undefined && phone_number.length > 15)
     ) {
        return { state: false, errorMsg: "Phone number is invalid" };
     }

     

     return { state: true };
   
   }
   

   edit() {
      let validated = this.validateData();
      if (validated.state) {
        const {
         rid,
         name,
         email,
         phone_number
        } = this.data;

        Referral.update(
         {
            phone_number:phone_number,
            name:name,
            email:email},
         {where: {rid:rid}}
        ).then(updated=>{
         let responseData = JSON.stringify({
            statusMsg: "Realtor Referral Updated Successfully."
         });
         return ResponseObj.responseHandlers(200, this.res, responseData);
        });
         
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Edit;
