const ResponseObj = require("../../utilities/responsehandler");
let Bank = require("../../database/realtor_account_details");
let validator = require("validator");

class Edit {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.edit();
   }

   validateData() {
      
        const {
            rid,
            account_name,
            bank_name,
            account_number
            
        } = this.data;
      
      //rid validation
      if (rid == undefined) {
        return { state: false, errorMsg: "RID must be provided" };
     }

      // account_name validation
      if (
         (account_name != undefined && account_name == '') || (account_name != undefined && (account_name.indexOf('<') != -1 || account_name.indexOf('>') != -1 || account_name.indexOf('"') != -1 || account_name.indexOf('\'') != -1))
         ) {
        return { state: false, errorMsg: "Account Name not valid." };
     }

     //bank_name validation
     if (
      (bank_name != undefined && bank_name == '') || (bank_name != undefined && (bank_name.indexOf('<') != -1 || bank_name.indexOf('>') != -1 || bank_name.indexOf('"') != -1 || bank_name.indexOf('\'') != -1)) 
      ) {
       return { state: false, errorMsg: "Bank Name not valid." };
      }

     // account_number validation
     if (
      (account_number != undefined && account_number == '') || (account_number != undefined && (account_number.indexOf('<') != -1 || account_number.indexOf('>') != -1 || account_number.indexOf('"') != -1 || account_number.indexOf('\'') != -1)
       )) {
        return { state: false, errorMsg: "Account Number is not valid" };
     }

     return { state: true };
   
   }
   

   edit() {
      let validated = this.validateData();
      if (validated.state) {
        const {
            rid,
            account_name,
            bank_name,
            account_number
        } = this.data;

        Bank.update(
         {
            account_name:account_name,
            bank_name:bank_name,
            account_number:account_number},
         {where: {rid:rid}}
        ).then(updated=>{
         let responseData = JSON.stringify({
            statusMsg: "Realtor Bank Details Updated Successfully."
         });
         return ResponseObj.responseHandlers(200, this.res, responseData);
        });
         
      } else {
         let responseData = JSON.stringify({
            statusMsg: validated.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Edit;
