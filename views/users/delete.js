const ResponseObj = require("../../utilities/responsehandler");
let Users = require("../../database/users");

const toggleString = ["DEACTIVATE","ACTIVATE"];

class Delete {
   constructor(data, res) {
      this.data = data.body;
      this.res = res;
      this.doDelete();
   }

   validateData() {
      const { uid, action } = this.data;
      
      //uid validation
      if (uid == undefined) {
         return { state: false, errorMsg: "UID must be provided" };
      }

      if (toggleString.includes(action.toUpperCase()) == false) {
         return {
            state: false,
            errorMsg: "Invalid action flag"
         };
      }
      return {
         state: true
      };
   }

   doDelete() {
      let validate = this.validateData();
      if (validate.state) {
         const { uid, action } = this.data;
         Users.findOne({ where: { uid: uid } }).then(user => {
            if (user == null) {
               let responseData = JSON.stringify({
                  statusMsg: "User not found."
               });
               return ResponseObj.responseHandlers(400, this.res, responseData);
            }
            let status;
            // If toggle = 1, activate user
            if (action == "deactivate") {
               user.update({
                  status: false
               });
               status = "deactivated";
            }
            // if toggle = 0, delete user
            if (action == "activate") {
               user.update({
                  status: true
               });
               status = "activated";
            }

            let responseData = JSON.stringify({
               statusMsg: "User " + status
            });
            return ResponseObj.responseHandlers(200, this.res, responseData);
         });
      } else {
         let responseData = JSON.stringify({
            statusMsg: validate.errorMsg
         });
         return ResponseObj.responseHandlers(400, this.res, responseData);
      }
   }
}

module.exports = Delete;
