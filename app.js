const express = require("express");
// const session = require('express-session');
const bodyParser = require("body-parser");
const routeHandlers = require("./utilities/routehandler");
var formidable = require('formidable');
var fs = require('fs');

var cors = require('cors');

const AddUser = require("./views/users/add");
const GetUserType = require("./views/user_type/all");
const EditPassword = require("./views/users/editPassword");
const EditUser = require("./views/users/edit");
const DeleteUser = require("./views/users/delete");
const GetAdmin = require("./views/users/allAdmin");
const GetCustomer = require("./views/users/allCustomer");
const GetRealtor = require("./views/users/allRealtor");
const Login = require("./views/users/login");

const AddRole = require("./views/role/add");
const EditRole = require("./views/role/edit");
const GetRole = require("./views/role/all");
const DeleteRole = require("./views/role/delete");

const AddRule = require("./views/property/rules/add");
const AllRule = require("./views/property/rules/all");
const DeleteRule = require("./views/property/rules/delete");

const AddCancel = require("./views/property/cancel/add");
const AllCancel = require("./views/property/cancel/all");
const DeleteCancel = require("./views/property/cancel/delete");

const AllProperty = require("./views/property/all");
const DeleteProperty = require("./views/property/delete");

const AddPropertyLandInfo = require("./views/property/land-info/add");
const AddPropertyLocation = require("./views/property/location/add");
const EditPropertyLandInfo = require("./views/property/land-info/edit");
const EditPropertyLocation = require("./views/property/location/edit");
const AddPropertyFacilities = require("./views/property/facilities/add");
const AddPropertyPaymentPlan = require("./views/property/payment-plan/add");
const AllPropertyPaymentPlan = require("./views/property/payment-plan/all");
const EditPropertyPaymentPlan = require("./views/property/payment-plan/edit");
const DeletePropertyPaymentPlan = require("./views/property/payment-plan/delete");
const AllPropertyFacilities = require("./views/property/facilities/all");
const EditPropertyFacilities = require("./views/property/facilities/edit");
const AllDbPropertyFacilities = require("./views/property/facilities/db/all");
const AddDbPropertyFacilities = require("./views/property/facilities/db/add");
const EditDbPropertyFacilities = require("./views/property/facilities/db/edit");
const DeleteDbPropertyFacilities = require("./views/property/facilities/db/delete");

const BuyProperty = require("./views/property/buy/add");
const UpdateBuyProperty = require("./views/property/buy/update");

const AddInvoice = require("./views/invoice/add");
const EditInvoice = require("./views/invoice/edit");
const AllInvoice = require("./views/invoice/all");
const AllRealtorInvoice = require("./views/invoice/allrealtor");
const AllCustomerInvoice = require("./views/invoice/allcustomer");
const DeleteInvoice = require("./views/invoice/delete");

const AllAgreement = require("./views/Agreement/all");
const AllCustomerAgreement = require("./views/Agreement/allcustomer");

const AddReceipt = require("./views/receipt/add");
const EditReceipt = require("./views/receipt/edit");
const AllReceipt = require("./views/receipt/all");
const AllCustomerReceipt = require("./views/receipt/allcustomer");
const DeleteReceipt = require("./views/receipt/delete");

const AllState = require("./views/state/all");
const AllCountry = require("./views/country/all");
const AllPurchases = require("./views/purchases/all");
const AllCustomerPurchases = require("./views/purchases/allcustomer");

const RealtorTopPerformance = require("./views/users/realtortopperformance");

const AddInspection = require("./views/inspection/add");
const AllInspection = require("./views/inspection/all");
const AllCustomerInspection = require("./views/inspection/allcustomer");
const AllRealtorInspection = require("./views/inspection/allrealtor");
const DeleteInspection = require("./views/inspection/delete");

const DeleteDownline = require("./views/downline/delete");
const AllDownline = require("./views/downline/all");
const AddDownline = require("./views/downline/add");

const RealtorBank = require("./views/users/realtorbank");
const RealtorReferral = require("./views/users/realtorreferral");
const CustomerReferral = require("./views/users/customerreferral");

const EditRealtor = require("./views/users/editrealtor");
const EditCustomer = require("./views/users/editcustomer");

const AllPropertyRealtors = require('./views/property-realtors/all');
const AddPropertyRealtors = require('./views/property-realtors/add');
const DeletePropertyRealtors = require('./views/property-realtors/delete');

const GetStats = require('./views/dashboard/stats');
const GetStatsTopCustomers = require('./views/dashboard/topCustomers');
const GetStatsNewCustomers = require('./views/dashboard/newCustomers');
const GetStatsGeneral = require('./views/dashboard/general');
const AddVisitLog = require('./views/Log/visit');

const AddPaymentHistory = require('./views/Payment-History/add');
const UpdatePaymentHistory = require('./views/Payment-History/edit');
const AllPaymentHistory = require('./views/Payment-History/all');
const AllCustomerPaymentHistory = require('./views/Payment-History/allCustomer');
const DeletePaymentHistory = require('./views/Payment-History/delete');

const app = express();
const dotenv = require("dotenv");
const path = require("path");

const auth = require("./utilities/authenticate");
const syncDB = require("./utilities/syncDB");
let Property_Picture = require("./database/property_pictures");
let Property_Document = require("./database/property_document");


class SRServer {
    constructor() {
       this.initExpressRoute();
       this.initExpressStart();
    }
 
    initExpressRoute() {
       syncDB();
       app.use(bodyParser.urlencoded({ extended: true, limit: '50mb', extended: false, parameterLimit: 1000000 }));
       app.use(bodyParser.json({limit: '50mb'}));
       //var allowedOrigins = ['http://localhost:4200',
                      //'http://178.79.130.102', 'http://www.foanrealties.com', 'http://foanrealties.com', 'http://portal.foanrealties.com', 'http://www.portal.foanrealties.com'];
      
      // app.use(cors({
      // origin: function(origin, callback){
      //    // allow requests with no origin 
      //    // (like mobile apps or curl requests)
      //    if(!origin) return callback(null, true);
      //    if(allowedOrigins.indexOf(origin) === -1){
      //       var msg = 'The CORS policy for this site does not ' +
      //                'allow access from the specified Origin.';
      //       return callback(new Error(msg), false);
      //    }
      //    return callback(null, true);
      // }
      // }));

      app.use(cors());

       // Custom token authentication middleware
       app.use(auth);

 
       let routes = JSON.parse(routeHandlers.handleRoutes());
 
       app.get("/api/ping/", (req, res) => {
         res.setHeader('Content-Type', 'application/json');
         res.status(200);
         res.send(JSON.stringify({'statResponse': true}));
       });

      //USER
      app.post(routes.addUser, (req, res) => {
         new AddUser(req, res);
      });
      app.post(routes.editUser, (req, res) => {
         new EditUser(req, res);
      });
      app.post(routes.deleteUser, (req, res) => {
         new DeleteUser(req, res);
      });
      app.post(routes.getAdmin, (req, res) => {
         new GetAdmin(req, res);
      });
      app.post(routes.getCustomer, (req, res) => {
         new GetCustomer(req, res);
      });
      app.post(routes.getRealtor, (req, res) => {
         new GetRealtor(req, res);
      });
      app.post(routes.getStats, (req, res) => {
         new GetStats(req, res);
      });

      app.post(routes.login,(req, res) => {
         new Login(req, res);
      });
      app.post(routes.getUserType, (req, res) => {
         new GetUserType(req, res);
      });
      app.post(routes.editPassword, (req, res) => {
         new EditPassword(req, res);
      });

      //ROLES
      app.post(routes.addRole, (req, res) => {
         new AddRole(req, res);
      });
      app.put(routes.editRole, (req, res) => {
         new EditRole(req, res);
      });
      app.post(routes.getRole, (req, res) => {
         new GetRole(req, res);
      });
      app.delete(routes.deleteRole, (req, res) => {
         new DeleteRole(req, res);
      });

      //PROPERTY
      app.post(routes.allProperty, (req, res) => {
         new AllProperty(req, res);
      });
      app.delete(routes.deleteProperty, (req, res) => {
         new DeleteProperty(req, res);
      });
      
      app.post(routes.addPropertyLandInfo, (req, res) => {
         new AddPropertyLandInfo(req, res);
      });
      app.put(routes.editPropertyLandInfo, (req, res) => {
         new EditPropertyLandInfo(req, res);
      });
      app.post(routes.addPropertyLocation, (req, res) => {
         new AddPropertyLocation(req, res);
      });
      app.put(routes.editPropertyLocation, (req, res) => {
         new EditPropertyLocation(req, res);
      });
      app.post(routes.addPropertyFacilities, (req, res) => {
         new AddPropertyFacilities(req, res);
      });
      app.post(routes.allPropertyFacilities, (req, res) => {
         new AllPropertyFacilities(req, res);
      });
      app.put(routes.editPropertyFacilities, (req, res) => {
         new EditPropertyFacilities(req, res);
      });
      app.post(routes.addDbPropertyFacilities, (req, res) => {
         new AddDbPropertyFacilities(req, res);
      });
      app.put(routes.editDbPropertyFacilities, (req, res) => {
         new EditDbPropertyFacilities(req, res);
      });
      app.post(routes.allDbPropertyFacilities, (req, res) => {
         new AllDbPropertyFacilities(req, res);
      });
      app.delete(routes.deleteDbPropertyFacilities, (req, res) => {
         new DeleteDbPropertyFacilities(req, res);
      });

      app.post(routes.addPropertyPaymentPlan, (req, res) => {
         new AddPropertyPaymentPlan(req, res);
      });
      app.post(routes.allPropertyPaymentPlan, (req, res) => {
         new AllPropertyPaymentPlan(req, res);
      });
      app.put(routes.editPropertyPaymentPlan, (req, res) => {
         new EditPropertyPaymentPlan(req, res);
      });
      app.delete(routes.deletePropertyPaymentPlan, (req, res) => {
         new DeletePropertyPaymentPlan(req, res);
      });

      app.post(routes.allRule, (req, res) => {
         new AllRule(req, res);
      });
      app.post(routes.addRule, (req, res) => {
         new AddRule(req, res);
      });
      app.delete(routes.deleteRule, (req, res) => {
         new DeleteRule(req, res);
      });

      app.post(routes.allCancel, (req, res) => {
         new AllCancel(req, res);
      });
      app.post(routes.addCancel, (req, res) => {
         new AddCancel(req, res);
      });
      app.delete(routes.deleteCancel, (req, res) => {
         new DeleteCancel(req, res);
      });

      app.post(routes.addPropertyPicture, (req, res) => {
         var form = new formidable.IncomingForm();
         form.uploadDir = __dirname+'/images';
         form.keepExtensions = true;
         var pid = '';
         
         form.parse(req, (err, field, file)=>{
            
            if(field.pid == undefined || field.pid == '' ){
               res.setHeader('Content-Type', 'application/json');
               res.status(400);
               res.send(JSON.stringify({'statusMsg': 'Provide all mandatory fields'}));
            }else{
               
               var destpath = __dirname + '/images/'+field.pid+'/';
               
               //create property folder
               fs.mkdir(destpath, {recursive: true}, (err)=>{
                  
                  //move temp file to permanent location
                  fs.rename(file.image.path, destpath+file.image.name, (err)=>{
                     if(err) throw err;
                        fs.readdir(__dirname+'/images', (err, files)=>{
                        //if(err) throw err;
                        if(files.length>0){
                           files.forEach((val, ind)=>{
                              if(val.indexOf('.png') >= 0 || val.indexOf('.jpg') >= 0 || val.indexOf('.gif') >= 0 || val.indexOf('.svg')>= 0 || val.indexOf('.docx')>= 0 || val.indexOf('.xls')>= 0 || val.indexOf('.html'>= 0)>= 0 || val.indexOf('.js')>= 0){
                                 fs.unlink(__dirname+'/images/'+val);
                              }
                           });
                        }
                        //Add record of uploaded image to db
                        Property_Picture.create({
                           filename: file.image.name,
                           pid:field.pid
                        }).then(addImage => {
                           res.setHeader('Content-Type', 'application/json');
                           res.status(200);
                           res.send(JSON.stringify({'statusMsg': 'Uploaded File Successfully', 'file': file.image.name }));
                        });
                     });
                  });
                  
               });
            }
         });
         
      });

      //AGREEMENT
      app.post(routes.uploadAgreement, (req, res) => {
         var form = new formidable.IncomingForm();
         form.uploadDir = __dirname+'/property_documents';
         form.keepExtensions = true;
         var pid = ''; var doc_title = '';
         
         // form.parse(req);

         form.parse(req, (err, field, file)=>{
            if(field.pid == undefined || field.pid == '' || field.doc_title == undefined || field.doc_title == ''){
               res.setHeader('Content-Type', 'application/json');
               res.status(400);
               res.send(JSON.stringify({'statusMsg': 'Provide all mandatory fields'}));
            }else{
               
               var destpath = __dirname + '/property_documents/'+field.pid+'/';

               fs.mkdir(destpath, {recursive: true}, (err)=>{
                  if(err && err.code != 'EEXIST'){
                     res.setHeader('Content-Type', 'application/json');
                     res.status(400);
                     res.send(JSON.stringify({'statusMsg': 'An Error Occurred'}));
                  }
                  //move temp file to permanent location
                  fs.rename(file.image.path, destpath+file.image.name, (err)=>{
                     if(err) throw err;
                        fs.readdir(__dirname+'/property_documents', (err, files)=>{
                        if(files.length>0){
                           files.forEach((val, ind)=>{
                              if(val.indexOf('.png') >= 0 || val.indexOf('.jpg') >= 0 || val.indexOf('.gif') >= 0 || val.indexOf('.svg')>= 0 || val.indexOf('.docx')>= 0 || val.indexOf('.xls')>= 0 || val.indexOf('.html'>= 0)>= 0 || val.indexOf('.js')>= 0){
                                 fs.unlink(__dirname+'/property_documents/'+val);
                              }
                           });
                        }
                        //Add record of uploaded image to db
                        Property_Document.create({
                           filename: file.image.name,
                           pid:field.pid,
                           title:field.doc_title
                        }).then(addImage => {
                           res.setHeader('Content-Type', 'application/json');
                           res.status(200);
                           res.send(JSON.stringify({'statusMsg': 'Uploaded Document Successfully', 'file': file.image.name }));
                        });
                     });
                  });
                  
               });
            
            }

         });
         
      });
      app.delete(routes.deleteAgreement, (req, res) => {
         
         var reqq = req.body;
         const {pid, filename} = reqq;

         Property_Document.destroy({where:{pid:pid, filename:filename}}).then(deleted=>{
            //delete file from fileserver
            var filepath = __dirname + '/property_documents/'+pid+'/'+filename;
            fs.unlink(filepath, function (err) {
                if (err) {
                  res.setHeader('Content-Type', 'application/json');
                  res.status(400);
                  res.send(JSON.stringify({'statusMsg': 'An Error Occurred', 'err': err }));
                      
                }
               
               res.setHeader('Content-Type', 'application/json');
               res.status(200);
               res.send(JSON.stringify({'statusMsg': 'Agreement Document Deleted', 'file': filename }));
            }); 
         });
      });
      app.post(routes.allAgreement, (req, res) => {
         new AllAgreement(req, res);
         
      });
      app.post(routes.allCustomerAgreement, (req, res) => {
         new AllCustomerAgreement(req, res);
      });

      app.post(routes.buyProperty, (req, res) => {
         new BuyProperty(req, res);
      });
      app.post(routes.updateBuyProperty, (req, res) => {
         new UpdateBuyProperty(req, res);
      });

      //INVOICE
      app.post(routes.addInvoice, (req, res) => {
         new AddInvoice(req, res);
      });
      app.put(routes.editInvoice, (req, res) => {
         new EditInvoice(req, res);
      });
      app.delete(routes.deleteInvoice, (req, res) => {
         new DeleteInvoice(req, res);
      });
      app.post(routes.allInvoice, (req, res) => {
         new AllInvoice(req, res);
      });
      app.post(routes.allRealtorInvoice, (req, res) => {
         new AllRealtorInvoice(req, res);
      });
      app.post(routes.allCustomerInvoice, (req, res) => {
         new AllCustomerInvoice(req, res);
      });

      //RECEIPT
      app.post(routes.addReceipt, (req, res) => {
         new AddReceipt(req, res);
      });
      app.put(routes.editReceipt, (req, res) => {
         new EditReceipt(req, res);
      });
      app.delete(routes.deleteReceipt, (req, res) => {
         new DeleteReceipt(req, res);
      });
      app.post(routes.allReceipt, (req, res) => {
         new AllReceipt(req, res);
      });
      app.post(routes.allCustomerReceipt, (req, res) => {
         new AllCustomerReceipt(req, res);
      });

      //STATE
      app.post(routes.allState, (req, res) => {
         new AllState(req, res);
      });

      //COUNTRY
      app.post(routes.allCountry, (req, res) => {
         new AllCountry(req, res);
      });

      //PURCHASES
      app.post(routes.allPurchases, (req, res) => {
         new AllPurchases(req, res);
      });
      app.post(routes.allCustomerPurchases, (req, res) => {
         new AllCustomerPurchases(req, res);
      });

      //REALTOR
      app.post(routes.realtorTopPerformance, (req, res) => {
         new RealtorTopPerformance(req, res);
      });

      //INSPECTION
      app.post(routes.addInspection, (req, res) => {
         new AddInspection(req, res);
      });
      app.delete(routes.deleteInspection, (req, res) => {
         new DeleteInspection(req, res);
      });
      app.post(routes.allInspection, (req, res) => {
         new AllInspection(req, res);
      });
      app.post(routes.allCustomerInspection, (req, res) => {
         new AllCustomerInspection(req, res);
      });
      app.post(routes.allRealtorInspection, (req, res) => {
         new AllRealtorInspection(req, res);
      });

      //DOWNLINE
      app.delete(routes.deleteDownline, (req, res) => {
         new DeleteDownline(req, res);
      });
      app.post(routes.allDownline, (req, res) => {
         new AllDownline(req, res);
      });
      app.post(routes.addDownline, (req, res) => {
         new AddDownline(req, res);
      });

      app.post(routes.realtorReferral, (req, res) => {
         new RealtorReferral(req, res);
      });
      app.put(routes.realtorBank, (req, res) => {
         new RealtorBank(req, res);
      });
      app.post(routes.customerReferral, (req, res) => {
         new CustomerReferral(req, res);
      });

      app.post(routes.updateRealtor, (req, res) => {
         new EditRealtor(req, res);
      });
      app.post(routes.updateCustomer, (req, res) => {
         new EditCustomer(req, res);
      });

      // Property Realtor
      app.post(routes.allPropertyRealtors, (req,res)=>{
         new AllPropertyRealtors(req,res);
      });

      app.post(routes.addPropertyRealtors, (req,res)=>{
         new AddPropertyRealtors(req,res);
      });

      app.post(routes.deletePropertyRealtors, (req,res)=>{
         new DeletePropertyRealtors(req,res);
      });

      //STAT
      app.post(routes.addVisitLog, (req, res) => {
         new AddVisitLog(req, res);
      });
      app.post(routes.getStatsTopCustomers, (req, res) => {
         new GetStatsTopCustomers(req, res);
      });
      app.post(routes.getStatsNewCustomers, (req, res) => {
         new GetStatsNewCustomers(req, res);
      });
      app.post(routes.getStatsGeneral, (req, res) => {
         new GetStatsGeneral(req, res);
      });

      //PROPERTY PAYMENT HISTORY
      app.post(routes.addPaymentHistory, (req, res) => {
         new AddPaymentHistory(req, res);
      });
      app.post(routes.updatePaymentHistory, (req, res) => {
         new UpdatePaymentHistory(req, res);
      });
      app.post(routes.allPaymentHistory, (req, res) => {
         new AllPaymentHistory(req, res);
      });
      app.post(routes.allCustomerPaymentHistory, (req, res) => {
         new AllCustomerPaymentHistory(req, res);
      });
      app.post(routes.deletePaymentHistory, (req, res) => {
         new DeletePaymentHistory(req, res);
      });

    }
 
    initExpressStart() {
       dotenv.config({ path: path.join(__dirname, ".env") });
 
       const port = process.env.PORT || 9000;
       app.listen(port, () => console.log(`Listen on port ${port}...`));
    }
 }
 
 new SRServer();